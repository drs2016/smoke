package smoke.objects;

import org.openqa.selenium.By;

public class ProfileMergePage {

	public static By ProfileMerge = By.id("ctl00_lnkMergeProfilesLink");;
	public static By ttMembershipEmail = By.xpath("//span[contains(text(),'TravelTracker Membership # used for')]");
	public static By lastName = By.id("ctl00_MainContent_txtLastName");
	public static By searchBtn = By.name("ctl00$MainContent$ctl00");
	public static By profileNames = By.id("ctl00_MainContent_lstMergeProfiles_ctrl0_ucMergeTravellerProfile_updatePnlMergeProfile");
	
	public static By mergeCheckBox = By.xpath("(//span[@class='chkSelectAll']/input)[last()]");
	public static By mergeBtn = By.xpath("(//input[@class='btnMerge'])[last()]");
	public static By mergeEditBtn = By.xpath("(//td[@class='btnEditProfile']/input)[last()]");
	public static By searchDetails = By.xpath("//div[@class='groupHeader']");
	public static By mergeProfileLoadingImage = By.id("//div[@id='ctl00_MainContent_lstMergeProfiles_ctrl0_ucMergeTravellerProfile_updatePnlMergeProfile']//div[@class='modal']//img");
	public static By mergeIndicator = By.xpath("//span[contains(text(),'Merged')]");
	public static By profileNamesCount = By.xpath("//*[@id='Div1']/div[1]/table/tbody//div[contains(@id,'ctrl')]//div[@class='groupHeader']");
	public static By profileNameInList = By.xpath("//*[@id='Div1']/div[1]/table/tbody//div[contains(@id,'ctrl<replaceValue>')]//div[@class='groupHeader']");
	public static By lastProfileName = By.xpath("(//*[@id='Div1']/div[1]/table/tbody//div[contains(@id,'ctrl')]//div[@class='groupHeader'])[last()]");
	public static By profileName = By.xpath("(//span[@id='spnName'])[last()]");
	public static By cntyName = By.xpath("(//span[contains(text(),'Home Country')])[1]/../../following-sibling::tr[2]/td");
}
