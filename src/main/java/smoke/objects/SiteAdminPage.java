package smoke.objects;

import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class SiteAdminPage {

	public static By siteAdminLink;
	public static By generaltab;
	public static By customerName;
	public static By selectcustomerDropDown;
	public static By myTripsTab;
	public static By manualTripEntryTab;
	public static By profileGrouptab;
	public static By profileFieldtab;
	public static By segmentationTab;
	public static By assignGrpUserTab;

	public static By selectuserDropdown;
	public static By selectuser;
	public static By mtqCaseNumberAvail;
	public static By mtqCaseNumberSelected;
	/**
	 * Below Locators are related to UI elements in the General tab of Site
	 * Admin page
	 */

	public static By userTab;
	public static By assignRolesToUsersTab;
	public static By assignUsersToRoleTab;
	public static By profileOptionsTab;
	public static By userMigrationTab;
	public static By promptedCheckInExclusionsTab;
	public static By selectUserUserTab;
	public static By userNameUserTab;
	public static By firstNameUserTab;
	public static By lastNameUserTab;
	public static By emailAddressUserTab;
	public static By emailStatus;
	public static By updateBtnUserTab;
	public static By successMsgUserTab;
	public static By selectUserRoleTab;
	public static By availableRole;
	public static By selectedRole;
	public static By appRoleName;
	public static By appRoleDesc;
	public static By appRoleDropDown;
	public static By roleDesc;
	public static By availableUsers;
	public static By selectedUsers;
	public static By profileField;
	public static By dropDownOptions;
	public static By migrateUsersBtn;
	public static By headerMsg;
	public static By progressImage;

	/**
	 * Below Locators are related to UI elements in the Manual Trip Entry tab of
	 * Site Admin page
	 */
	public static By tripSegmentsTab;
	public static By unmappedProfileFieldsTab;
	public static By metadataTripQuestionTab;
	public static By metadataTripQuestionTabMyTrips;
	public static By availableMetadataTripQstnsMyTripsTab;
	public static By customTripQuestionTab;
	public static By selectProfileGroup;
	public static By profileGroupLabel;
	public static By updateLabelButton;
	public static By groupNameSuccessMsg;
	public static By profileGroupSaveBtn;
	public static By profileGroupSuccessMsg;
	public static By attributeGroup;
	public static By availableProfileFields;
	public static By selectedProfileFields;
	public static By labelForProfileField;
	public static By businessLocation;
	public static By isRequiredOnFormCheckbox;
	public static By profileFieldHomeSite;
	public static By profileFieldUpdateLabel;
	public static By profileFieldUpdateSuccessMsg;
	public static By profileFieldSaveBtn;
	public static By profileFieldSuccessMsg;
	public static By availableSegments;
	public static By selectedSegments;
	public static By availableFields;
	public static By selectedFields;
	public static By removeBtn;
	public static By availableFieldsValue;
	public static By selectedFieldsValue;
	public static By addBtn;
	public static By umappedProFldSaveBtn;
	public static By umappedProFldSuccessMsg;
	public static By availableMetadataTripQstns;
	public static By selectedMetadataTripQstns;
	public static By selectedMetadataTripQstnsMyTrips;
	public static By labelName;
	public static By responseType;
	public static By labelNameMyTrips;
	public static By responseTypeMyTrips;
	public static By newBtn;
	public static By defaultQstn;
	public static By qstnText;
	public static By responseTypeCTQ;
	public static By applySettings;
	public static By updateCustomTripQstn;
	public static By saveChangesBtn;
	public static By CTQSuccessMsg;

	/**
	 * Below Locators are related to UI elements in the Segmentation tab of Site
	 * Admin page
	 */

	public static By assignUserToGrpTab;
	public static By addEditGrpTab;
	public static By addEditFilterTab;
	public static By assignFiltersToGrpTab;
	public static By selectGrpDropdown;
	public static By saveaddEditGrpBtn;
	public static By addEditGrpSuccessMsg;
	public static By filterDropdown;
	public static By saveaddEditFilterBtn;
	public static By addEditFilterSuccessMsg;
	public static By grpDropdown;
	public static By availableFilter;
	public static By moveFilter;
	public static By saveAssignToFiltersBtn;
	public static By assignFiltersToGrpSuccessMsg;
	public static By user;
	public static By availableGrp;
	public static By moveGrp;
	public static By saveAssignGrpToUserBtn;
	public static By assignGrpToUserSuccessMsg;
	public static By group;
	public static By availableUser;
	public static By moveUser;
	public static By saveAssignUserToGrpBtn;
	public static By assignUserToGrpSuccessMsg;

	/**
	 * Below Locators are related to users tab inside General tab of Site Admin
	 * Page.
	 */
	public static By emulateUserBtn;
	public static By welcomeCust;
	public static By stopEmulationBtn;
	/*
	 * 
	 */
	public static By assistanceAppCheckBox;
	public static By updateBtnCustTab;
	public static By checkInsTab;
	public static By blueManImagecheckInsTab;
	public static By customerNameDynamic;
	public static By oneWaySMSEnabled;
	public static By twoWaySMSEnabled;
	public static By expatEnabledCheckBox;

	// Sep 8
	public static By employeeID;
	public static By btnRemoveArrow;

	public static By tabRole;
	public static By appDropdown;
	public static By roleDropdown;
	public static By roleDescription;
	public static By roleChkBox;
	public static By roleSaveBtn;
	public static By roleTextBox;

	public static By applicationTab;
	public static By appNameFieldText;
	public static By appEnableChkBox;
	public static By appSaveBtn;
	public static By appStatusMsg;
	public static By appSelDropDown;
	public static By appUpdateBtn;

	public static By tabFeature;
	public static By featureAppDropdown;
	public static By featureTextBox;
	public static By featureDesp;
	public static By featureChkBox;
	public static By featureSaveBox;
	public static By featureSelectDropdown;

	public static By vismoEnabledCheckbox;
	public static By isosResourcesEnabledCheckbox;
	
	//sep 20
	public static By customRiskRatingsEditor;
	public static By officeLocations;
	public static By officeLocations_ReadOnly;
	public static By profileMergeUser;
	public static By btnRemove;
	public static By updateBtn;
	public static By assistanceAppCheckinsEnabledCheckBox; 
	public static By btnAdd;
	public static By officeLocationsAvailable;
	public static By customerTab;
	public static By officeLocationsReadOnlyAvailable;
	public static By profileMergeUserAvailable;
	public static By editProgressImage;
	
	//oct 7
	public static By profileFieldCancelBtn;
	public static By profileFieldsAddBtn;
	public static By profileFieldRemoveBtn;
	public static By informationText;
	public static By tripSegementAddBtn;
	public static By tripSegementRemoveBtn;
	public static By tripSegementSaveBtn;
	public static By tripSegementCanceltn;
	public static By specialCharactersLabel;
	
	//Oct 10
	public static By availableSegments_MyTrips;
	public static By selectedSegments_MyTrips;
	public static By addBtn_MyTrips;
	public static By removeBtn_MyTrips;
	public static By saveBtn_MyTrips;
	public static By cancelBtn_MyTrips;
	public static By tripSegmentsTab_MyTrips;
	public static By updateEmailAddress;
	public static By updateEmailAddressSaveBtn;
	public static By updateEmailAddressSuccessMsg;
	
	
	public static By promptedChkBox;
	public static By vismoChkBox;
	public static By buildingsChkBox;
	public static By pTAChkBox;
	public static By pTLChkBox;
	public static By iSOSChkBox;
	public static By commsChkBox;
	public static By updateBtninCust;
	public static By custTabSuccessMsg;
	public static By vipStatusCheckbox;
	public static By ticketedStatusCheckbox;
	
	public static By uberCheckboxinSiteAdmin;
	public static By travelReadyComplianceStatusAvailableRole;
	public static By travelReadyAdminAvailableRole;
	public static By travelReadyComplianceStatusSelectedRole;
	public static By travelReadyAdminSelectedRole;
	

	//Need To Checkin
		public static By mtqBtninMTE;
		public static By mtqRightDropdown;
		public static By mtqRightDropdownMytrips;
		public static By mtqLeftDropdownMytrips;
		public static By removeBtnMytrips;
		public static By addBtnMytrips;
		public static By mtqLeftDropdownOptionsMyTrips;
		
		public static By mtqLeftDropdown;
		public static By mtqDropdownRightMoveArrow;
		public static By updateMetadataQsn;
		public static By mtqSaveBtn;
		
		public static By updateMTQBtn;
		public static By updateMTQBtnMytrips;
	    public static By attributeSuccessMsg;
	    public static By attributeSuccessMsgMytrips;

	    public static By saveMTQ;
	    public static By saveMTQMytrips;
	    public static By responseDropdown;
	    public static By availableCustomTripQstn;
	    public static By rightArrow;
	    public static By selectedCustomTripQstn;
	    public static By twentyQtnLimitError;

	    public static By segmentableAttribute;
		public static By filterName;
		public static By filterDesc;
		public static By customSegmentationValue;
		public static By blankSegmentationValue;
		public static By groupName;
		public static By groupDesc;
		public static By saveProfileOptions;
		public static By customValue;
		
		public static By agencyAvailableMTQ;
		public static By agencySelectedMTQ;
		public static By leftArrow;
		
		public static By selectedGrp;
		public static By RemoveBtninAssignGrptoUsr;
		public static By availableGroup;
		
		public static By selectGrpDrpdown;
		public static By slctedUsers;
		public static By avlableUser;
		
		public static By customersSegmentationGroupsChkBox;
		
		public static By assignGrpToUser;
		public static By assignGrpToUserRemoveBtn;
		public static By mapHomeLocations;
		
		public static By avlableFilter;
		public static By slctdFilter;
		public static By btnAddinGrouptoFilter;
		public static By btnRemoveinGrouptoFilter;
	
		//Dec 26
		public static By uberEnabledCheckBox;
		
		public static By labelofProActiveEmailServicelink;
		public static By labelofProActiveEmailServiceChkBox;
		public static By labelofActivelyTTHRUploadProcessChkBox;
		
		public static By btnRemoveinAssignUserstoRoleTab;
		public static By btnAddinAssignUserstoRoleTab;
		public static By btnSaveinAssignUserstoRoleTab;
		public static By lblMessageinAssignUserstoRoleTab;
		
		
		public static By asstAppCheckIns;	
		public static By myTripsUserAvailableRole;
		public static By myTripsUserSelectedRole;
		public static By myTripsUserAutomationAvailableRole;
		public static By myTripsUserAutomationSelectedRole;
		
		public static By mteUser;
		public static By labelAlreadyPresentError;
		public static By cancelBtnMyTrips;
		public static By loaderInCreateNewTraveller;		

		public static By userSaveBtn;
		public static By selectCustomerBox;
		
		public static By authorisedUser;
		public static By roleFeatureAssignTab;
		public static By everBridgeUserIdLabel;
		public static By everBridgePasswdLabel;
		public static By everBridgeUserIdTextBox;
		public static By everBridgePasswdTextBox;
		
		public static By everBridgeUserID;
		public static By everBridgePwd;
        public static By userRolesUpadated;
        
        public static By asgnGrpToUser;
        public static By removeGrp;
        public static By availableFilterinFltrinGrp;
        
        public static By availableCtryList;
        public static By addBtnInFilterTab;
        public static By saveBtnFilterTab;
        public static By saveMsgInFilterTab;
        public static By customerDetailsInCustomerTab;
        public static By manualtripentrylabel;
        public static By manualtripentrycheckbox;
        public static By everBridgeFieldsEmptyError;
        public static By loadingSpinner;
		public static By leftArrowCTQ;

		public static By salesForceLabel;
		public static By salesForceIDInput;
		public static By salesForceErrorMsg;
		public static By salesForceSuccessMsg;
		public static By cancelBtnCustTab;
		
		public static By ttIncidentSupportChkBox;
		public static By ttIncidentSupport;
		
		public static By customerSgementationGroup;
		

	public void siteAdminPage()

	{
		customerSgementationGroup = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_chkUserSgmentation']");
		selectuserDropdown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser");
		selectuser = By.xpath("//option[text()='<replaceValue>']");
		siteAdminLink = By.xpath("//a[text()='Site Admin']");
		generaltab = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_labelGeneral']");
		selectcustomerDropDown = By
				.xpath("//button[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_Button']");
		customerName = By.xpath("//li[text()='International SOS']");
		customerNameDynamic = By.xpath("//li[contains(text(),'<replaceValue>')]");
		myTripsTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkPTL']");
		manualTripEntryTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkMTE']");
		profileGrouptab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE']");
		profileFieldtab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE']");
		segmentationTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkSegmentation']");
		assignGrpUserTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser']");
		userTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser']");
		selectUserUserTab = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser']");
		userNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtUserName']");
		firstNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtFirstName']");
		lastNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtLastName']");
		emailAddressUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtEmailAddress']");
		emailStatus = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_rdStatus_0']");
		updateBtnUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnUserSave']");
		successMsgUserTab = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_lblMessage']");
		assignRolesToUsersTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment']");
		selectUserRoleTab = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser']");
		availableRole = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']");
		selectedRole = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']");
		appRoleName = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtRoleName']");
		appRoleDesc = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtDescription']");
		assignUsersToRoleTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole']");
		appRoleDropDown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_drpUserRole']");
		roleDesc = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_txtDescription']");
		availableUsers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstAvailableUsers']");
		selectedUsers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstSelectedUsers']");
		profileOptionsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions']");
		profileField = By.xpath("//select[@id='drpProfileField']");
		dropDownOptions = By.xpath("//input[@id='txtOptions']");
		userMigrationTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration']");
		migrateUsersBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration_ctl01_btnMigrate']");
		promptedCheckInExclusionsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions']");
		headerMsg = By
				.xpath("//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions_panPromptedCheckInExclusions']/h3");
		progressImage = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_UpdateProgress1']");
		selectProfileGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_listBoxSelectedGroups']/option[1]");
		profileGroupLabel = By.xpath("//input[@id='txtLabelValue']");
		updateLabelButton = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnUpdateAttributeGroupLabel']");
		groupNameSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_lblLabelMsg']");
		profileGroupSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnSave']");
		profileGroupSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_labelErrorMessage']");
		attributeGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_ddlAttributeGroup']");
		availableProfileFields = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_lstbAvailableProfileAttributes']");
		selectedProfileFields = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']");
		labelForProfileField = By.xpath("//input[@id='txtFieldValue']");
		businessLocation = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[1]");
		isRequiredOnFormCheckbox = By
				.xpath("//input[@id='chkIsRequiredProfileField']");
		profileFieldHomeSite = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[4]");
		profileFieldUpdateLabel = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnUpdateLabel']");
		profileFieldUpdateSuccessMsg = By
				.xpath("//span[@id='ProfileAttributelblLabelMsg']");
		profileFieldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnSave']");
		profileFieldSuccessMsg = By.xpath("//span[@id='lblMessage']");
		tripSegmentsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE']");
		availableSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxAvailableSegment']");
		selectedSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxSelectedSegment']");
		unmappedProfileFieldsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE']");
		availableFields = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_panelProfileExtendedAttributesGroupsMTE']/table//tr[1]/td/table/tbody/tr[2]/td[1]");
		selectedFields = By
				.xpath("//select[@id='listBoxSelectedExtendedAttributes']");
		availableFieldsValue = By
				.xpath("//select[@id='listBoxAvailableExtendedAttributes']/option[1]");
		selectedFieldsValue = By
				.xpath("//*[@id='listBoxSelectedExtendedAttributes']/option[1]");
		removeBtn = By.xpath("//*[@id='btnRemove']");
		addBtn = By.xpath("//input[@id='btnAdd']");
		umappedProFldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_ctl01_btnSave']");
		umappedProFldSuccessMsg = By
				.xpath("//span[@id='labelExtendedAttributeErrorMessage']");
		metadataTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE']");
		metadataTripQuestionTabMyTrips = By.cssSelector("#__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta");
		availableMetadataTripQstnsMyTripsTab = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblAvailableAttributes");
		availableMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes']");
		selectedMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes']");
		selectedMetadataTripQstnsMyTrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblSelectedAttributes");
		labelName = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_txtLabelValue']");
		responseType = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_ddlResponseType']");
		labelNameMyTrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_txtLabelValue");
		responseTypeMyTrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_ddlResponseType");
		
		customTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE']");
		newBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnNew']");
		defaultQstn = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Default Question Text']");
		qstnText = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_txtCustomTripQuestionValue']");

		responseTypeCTQ = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_ddlResponseType']");
		applySettings = By.xpath("//input[@id='chkSetOnToClone']");
		updateCustomTripQstn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnUpdateLabel']");
		saveChangesBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnSave']");
		CTQSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lblMessage']");

		assignUserToGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup']");
		addEditGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup']");
		addEditFilterTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter']");
		assignFiltersToGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup']");
		selectGrpDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_drpGroup']");
		saveaddEditGrpBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_btnSave']");
		addEditGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_lblMessage']");
		filterDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_drpFilter']");
		saveaddEditFilterBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_btnFilter']");
		addEditFilterSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_lblErrorMessage']");
		grpDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_drpGroup']");
		availableFilter = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']/option[text()='<replaceValue>']");
		moveFilter = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnAdd']");
		saveAssignToFiltersBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnSave']");
		assignFiltersToGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lblMessage']");
		user = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_drpUser']");
		availableGrp = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstAvailableGroup']/option[text()='<replaceValue>']");
		moveGrp = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnAdd']");
		saveAssignGrpToUserBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnSave']");
		assignGrpToUserSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lblMessage']");
		group = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_drpGroup']");
		availableUser = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']");
		moveUser = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnAdd']");
		saveAssignUserToGrpBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnSave']");
		assignUserToGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lblMessage']");

		emulateUserBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStartEmulation']");
		welcomeCust = By.xpath(".//span[contains(text(),'Welcome')]");
		stopEmulationBtn = By
				.xpath(".//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStopEmulation']");

		assistanceAppCheckBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkLocationTrackingEnabled");
		updateBtnCustTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
		checkInsTab = By.id("checkInsTab");
		blueManImagecheckInsTab = By.xpath("//img[contains(@src,'blue_man')]");
		oneWaySMSEnabled = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk1waySMS");
		twoWaySMSEnabled = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk2waySMS");
		expatEnabledCheckBox = By.xpath("//*[contains(@id,'chkExpatEnabled')]");

		employeeID = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[text()='Employee ID']");
		btnRemoveArrow = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnRemove");

		tabRole = By
				.xpath(".//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole']");
		appDropdown = By
				.name("ctl00$ContentPlaceHolder1$MainContent$tabContainerGeneral$tabRole$ctl01$ddlApplication");
		roleDropdown = By
				.name("ctl00$ContentPlaceHolder1$MainContent$tabContainerGeneral$tabRole$ctl01$ddlSelectRole");
		roleDescription = By
				.name("ctl00$ContentPlaceHolder1$MainContent$tabContainerGeneral$tabRole$ctl01$txtDescription");
		roleChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_chkActive");
		roleSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_btnSave");
		roleTextBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_txtRoleName");

		applicationTab = By.xpath("//span[text()='Application']");
		appNameFieldText = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_txtApplicationName");
		appEnableChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_chkActive");
		appSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_btnSave");
		appStatusMsg = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_lblErrorMsg");
		appSelDropDown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_ddlApplication");
		appUpdateBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_btnSave");

		tabFeature = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature");
		featureAppDropdown = By
				.name("ctl00$ContentPlaceHolder1$MainContent$tabContainerGeneral$tabFeature$ctl01$ddlApplication");
		featureTextBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_txtFeatureName");
		featureDesp = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_txtDescription");
		featureChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_chkActive");
		featureSaveBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_btnSave");
		featureSelectDropdown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_ddlSelectFeature");
		vismoEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkVismoEnabled");
		isosResourcesEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkSOSLocationEnabled");
		customRiskRatingsEditor = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Custom Risk Ratings Editor']");
		officeLocations = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Office Locations']");
		officeLocations_ReadOnly = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Office Locations (Read-Only)']");
		profileMergeUser = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Profile Merge User']");
		btnRemove = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnRemove");
		updateBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnSave");
		assistanceAppCheckinsEnabledCheckBox = By.xpath("//*[contains(@id,'chkLocationTrackingEnabled')]");
		btnAdd = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnAdd");
		officeLocationsAvailable = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Office Locations']");
		customerTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient");
		
		officeLocationsReadOnlyAvailable = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Office Locations (Read-Only)']");
		profileMergeUserAvailable = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Profile Merge User']");
		editProgressImage = By.id("ctl00_MainContent_UpdateProgressProfile");
		
		profileFieldCancelBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnCancel");
		profileFieldsAddBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnAdd");
		profileFieldRemoveBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnRemove");
		informationText = By.xpath("//td[contains(text(),'Important')]");
		tripSegementAddBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnAdd");
		tripSegementRemoveBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnRemove");
		tripSegementSaveBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnSave");
		tripSegementCanceltn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnCancel");
		specialCharactersLabel = By.xpath("//td[contains(text(),'Contains 1 or more special characters:')]");
		
		availableSegments_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxAvailableSegment");
		selectedSegments_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxSelectedSegment");
		addBtn_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnAdd");
		removeBtn_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnRemove");
		saveBtn_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnSave");
		cancelBtn_MyTrips = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnCancel");
		tripSegmentsTab_MyTrips = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment");
		updateEmailAddress = By.id("ctl00_MainContent_UpdateUserFields1_txtEmailAddress");
		updateEmailAddressSaveBtn = By.id("ctl00_MainContent_UpdateUserFields1_btnSave");
		updateEmailAddressSuccessMsg = By.id("ctl00_MainContent_UpdateUserFields1_lblNotification");
		
		
		promptedChkBox = By.xpath("//*[contains(@id,'chkPromptedCheckinsEnabled')]");
		vismoChkBox = By.xpath("//*[contains(@id,'chkVismoEnabled')]");
		buildingsChkBox = By.xpath("//*[contains(@id,'chkOfficeLocationsEnabled')]");
		pTAChkBox = By.xpath("//*[contains(@id,'chkPTAOnly')]");
		pTLChkBox = By.xpath("//*[contains(@id,'chkPTLClient')]");
		iSOSChkBox = By.xpath("//*[contains(@id,'chkSOSLocationEnabled')]");
		commsChkBox = By.xpath("//*[contains(@id,'chkCommsAddOn')]");
		updateBtninCust = By.xpath("//*[contains(@id,'btnSave')]");
		custTabSuccessMsg = By.xpath(".//span[@id='lblMessage']");
		vipStatusCheckbox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkDisplayVipFilter");
		ticketedStatusCheckbox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkShowTicketFilter");
		
		uberCheckboxinSiteAdmin = By.xpath("//label[text()='Uber Enabled']/preceding-sibling::input");
		travelReadyComplianceStatusAvailableRole = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - Compliance Status']");
		travelReadyAdminAvailableRole = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - TravelReady Admin']");
		travelReadyComplianceStatusSelectedRole = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - Compliance Status']");
		travelReadyAdminSelectedRole = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - TravelReady Admin']");
		
		
		mtqBtninMTE = By.xpath("//span[text()='Metadata Trip Question']");
		mtqRightDropdown = By.xpath("//select[contains(@id,'lstbAvailableProfileAttributes')]/option[1]");
		mtqLeftDropdown = By.xpath("//select[contains(@id,'lstbSelectedProfileAttributes')]/option[last()]");
	    mtqRightDropdownMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes>option:nth-child(1)");
		mtqLeftDropdownMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes>option:last-child");
		removeBtnMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnRemove");
		addBtnMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnAdd");
		mtqDropdownRightMoveArrow = By.xpath("//input[@value='>>']");
		updateMetadataQsn = By.xpath("//input[@value='Update Metadata Trip Question']");
		mtqSaveBtn = By.xpath("//input[@value='Save']");
				
		updateMTQBtn = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnUpdateLabel']");
		updateMTQBtnMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnUpdateLabel");
		attributeSuccessMsg = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lblLabelMsg");
		attributeSuccessMsgMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblLabelMsg");

		saveMTQ = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnSave");
		saveMTQMytrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnSave");
		responseDropdown = By.xpath("//select[contains(@id,'ddlResponseType')]");
		availableCustomTripQstn = By
		    .xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbAvailableProfileAttributes']/option[1]");
		rightArrow = By
		    .id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnAdd");
		selectedCustomTripQstn = By
		    .xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[last()]");
		segmentableAttribute = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_drpAttribute");
		filterName = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtName");
		filterDesc = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtDescription");
		customSegmentationValue = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_rdSegmentValueType_1");
		groupName = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_textGroupName");
		groupDesc = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_textDescription");
		saveProfileOptions = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions_ctl01_btnSave");
		customValue = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtValue");
mtqLeftDropdownOptionsMyTrips = By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes option:nth-child(1)");
		
		mtqCaseNumberAvail =By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes']/option[contains(text(),'Case Number')]");
		mtqCaseNumberSelected = By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes']/option[contains(text(),'Case Number')]");
		blankSegmentationValue = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_rdSegmentValueType_0");
		agencyAvailableMTQ = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes']/option[text()='Agency']");
		agencySelectedMTQ = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Agency']");
		leftArrow = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnRemove");
		leftArrowCTQ = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnRemove");
		availableGroup = By.xpath("//select[contains(@id,'lstAvailableGroup')]");
		selectedGrp = By.xpath("//select[contains(@id,'lstSelectedGroup')]");
		RemoveBtninAssignGrptoUsr = By.xpath("//input[contains(@id,'btnRemove')]");
		
		selectGrpDrpdown = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_drpGroup']");
		slctedUsers = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstSelectedUsers']");
		avlableUser = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']");
		
		customersSegmentationGroupsChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_chkUserSgmentation");
		
		assignGrpToUser = By.xpath(".//select[contains(@id,'lstSelectedGroup')]");
		//assignGrpToUserRemoveBtn = By.xpath("//input[@id='ctl00_MainContent_AssignGroupToUser1_btnRemove']");
		assignGrpToUserRemoveBtn = By.xpath("//input[@value='<<']");
		mapHomeLocations = By.xpath("//div[@class='locationPeopleDiv']");
		
		avlableFilter = By.xpath("//select[contains(@id,'lstAvailableFilters')]");
		slctdFilter = By.xpath("//select[contains(@id,'lstSelectedFilter')]");
		btnAddinGrouptoFilter = By.xpath("//input[contains(@id,'btnAdd')]");
		btnRemoveinGrouptoFilter = By.xpath("//input[contains(@id,'btnRemove')]");
		uberEnabledCheckBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkUberEnabled");
		
		labelofProActiveEmailServicelink = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_Label1");
		labelofProActiveEmailServiceChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_lblTT6ProactiveEmail");
		labelofActivelyTTHRUploadProcessChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkUploadsHRData");
		
		btnRemoveinAssignUserstoRoleTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnRemove");
		btnAddinAssignUserstoRoleTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnAdd");
		
		btnSaveinAssignUserstoRoleTab= By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnSave");
		lblMessageinAssignUserstoRoleTab= By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lblMessage");
	
		
		asstAppCheckIns = By.xpath("//label[text()='Assistance App Check-Ins Enabled']/..//input");
		
		
				mteUser = By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - MTE User']");
		myTripsUserAvailableRole =By.xpath
				("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='MyTrips - MyTripsUser']");
		myTripsUserSelectedRole =By.xpath
		("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='MyTrips - MyTripsUser']");
		myTripsUserAutomationAvailableRole=By.xpath
		("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='MyTrips - MyTripsUser Automation']");
		myTripsUserAutomationSelectedRole=By.xpath
		("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='MyTrips - MyTripsUser Automation']");
		
		labelAlreadyPresentError=By.xpath("//span[contains(text(),'Label name already exist in selected list')]");
		cancelBtnMyTrips=By.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnCancel']");
		loaderInCreateNewTraveller = By.xpath("//img[contains(@src,'load')]");
		
		userSaveBtn = By.xpath("//input[@value='Save']");
		selectCustomerBox = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']");
		
		authorisedUser = By.xpath("//span[text()='Authorised Users']");	
		roleFeatureAssignTab = By.xpath("//span[text()='Role Feature Assignment']");
		everBridgeUserIdLabel=By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label11");
		everBridgePasswdLabel=By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label12");   
		everBridgeUserIdTextBox=By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId"); 
		everBridgePasswdTextBox=By.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBPassword");
		everBridgeFieldsEmptyError=By.xpath("//span[@id='lblMessage' and contains(text(),'Please enter both Everbridge Account User Id and Password.')]");
		
		userRolesUpadated = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lblErrorMsg");
		everBridgeUserID = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId']");
		everBridgePwd = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBPassword']");
		
		
		asgnGrpToUser = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstSelectedGroup']/option[text()='<replaceValue>']");
		removeGrp = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnRemove']");
		availableFilterinFltrinGrp = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']");
		
		availableCtryList = By.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_lstAvailableCountry']");
		addBtnInFilterTab = By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_btnAdd']");
		saveBtnFilterTab = By.xpath("//input[@value='Save']");
		saveMsgInFilterTab = By.xpath("//span[contains(text(),'International SOS Add/Edit Filter updated.')]");
		customerDetailsInCustomerTab = By.xpath("//span[contains(text(),'Customer Details')]");
		
		manualtripentrycheckbox=By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkManuallyEnteredTrips']");
		manualtripentrylabel=By.xpath("//label[text()='Manually Entered Trips");
		loadingSpinner=By.xpath("//span[contains(text(),'Loading...')]");

		twentyQtnLimitError=By.xpath("//span[contains(text(),'You cannot add more than 20 questions  ')]");

		salesForceLabel=By.xpath("//span[contains(text(),'Salesforce Account ID')]");
		salesForceIDInput=By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtSalesforceAccountId']");
		salesForceErrorMsg=By.xpath("//span[contains(text(),'Numeric values only, no more then 10 numbers.')]");
		salesForceSuccessMsg=By.xpath("//span[contains(text(),'Modified By')]");
		cancelBtnCustTab=By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnCancel");
		
		ttIncidentSupport = By.xpath("//label[contains(text(),'TravelTracker Incident Support')]");
		ttIncidentSupportChkBox = By.xpath("//input[contains(@id,'chkTTManagedSupport')]");
	}

	/**
	 * This function verifies the positive flow of Saleforce account ID field
	 * @param ID
	 * @return
	 * @throws Throwable
	 *//*
	public Boolean verifySalesForcePositive(String ID) throws Throwable{
		Boolean flag=false;
		try{
			List<Boolean> flags=new ArrayList<>();

			LOG.info("verifySalesForcePositive function has started.");

			//Get the original Salesforce ID
			LOG.info("Verifying Salesforce ID field with ID:"+ID);
			String orgID=getAttributeByValue(SiteAdminPage.salesForceIDInput,
					"Enter Sales Force Account ID.");
			LOG.info("Org ID:"+orgID);

			//Enter the account ID and click Update Button
			flags.add(type(SiteAdminPage.salesForceIDInput, ID,
					"Enter Sales Force Account ID."));
			flags.add(click(SiteAdminPage.updateBtnCustTab,
					"Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			//Verify whether Modified By text is displayed if ID is not empty
			if(!ID.isEmpty()) {
				flags.add(assertElementPresent(SiteAdminPage.salesForceSuccessMsg,
						"Modified By message."));
				LOG.info(getText(SiteAdminPage.salesForceSuccessMsg,
						"Modified By message."));
			}
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg,
					"Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg,
					SiteAdminLib.verifySiteAdminpageCustomerName+" Customer updated.",
					"Assert success message")); //Customer name saved from the cmp verifySiteAdminpage

			//Verify whether the A/C Id field is Updated accordingly,
			// if blank field was entered then original Id should be displayed else the new ID should be displayed
			if(ID.isEmpty()){
				flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput,
						orgID,"Verify ID is still Present"));
				flag=true;
			}else{
				flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput,
						ID.trim(),"ID is Updated accordingly."));
				flag=true;
			}
			LOG.info("verifySalesForcePositive function has completed.");
			if(flags.contains(false)){
				LOG.error(flags);
				throw new Exception();
			}
		}catch (Exception e){
			LOG.error("verifySalesForcePositive function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	*//**
	 * This function verifies the negative flows and checks for the correct error messages
	 * @param ID
	 * @return
	 * @throws Throwable
	 *//*
	public Boolean verifySalesForceNegative(String ID) throws Throwable{
		Boolean flag = false;
		try{
			List<Boolean> flags=new ArrayList<>();
			LOG.info("verifySalesForceNegative function has started.");
			LOG.info("Verifying Salesforce ID field with ID:"+ID);

			//Get the original Salesforce ID
			String orgID=getAttributeByValue(SiteAdminPage.salesForceIDInput,
					"Enter Sales Force Account ID.");
			LOG.info("Org ID:"+orgID);
			//Enter the account ID and click Update Button
			flags.add(type(SiteAdminPage.salesForceIDInput, ID.trim(),
					"Enter Sales Force Account ID."));
			flags.add(click(SiteAdminPage.updateBtnCustTab,
					"Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			//Verify whether Error Message is displayed
			flags.add(assertElementPresent(SiteAdminPage.salesForceErrorMsg,
					"Error message."));
			LOG.info(getText(SiteAdminPage.salesForceErrorMsg,
					"Error message."));

			//Click on Cancel button and verify whether the ID is reverted to the original state
			flags.add(click(SiteAdminPage.cancelBtnCustTab,
					"Cancel button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput,
					orgID,"Verify whether Original ID is still Present"));

			//Set flag to true indicating Success
			flag = true;

			LOG.info("verifySalesForceNegative function has completed.");
			if(flags.contains(false)){
				LOG.error(flags);
				throw new Exception();
			}
		}catch (Exception e){
			LOG.error("verifySalesForceNegative function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	*//**
	 * This function verifies whether the modified Salesforce ID is reverted back to the Original value after 5 min
	 * @param originalID
	 * @return
	 * @throws Throwable
	 *//*
	public Boolean verifySalesForceRevert(String originalID) throws Throwable{
		Boolean flag = false;

		try{
			List<Boolean> flags=new ArrayList<>();
			LOG.info("verifySalesForceRevert function has started.");

			LOG.info("Waiting for 5 mins. and then check the ID field.");
			//Wait for 5 mins
			for(int i=0;i<30;i++) {
				Longwait();
			}

			//Click on User Tab
			isElementPresentWithNoException(SiteAdminPage.userTab);
			flags.add(JSClick(SiteAdminPage.userTab,
					"User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Loading Image");

			//Now click on Customer tab
			isElementPresentWithNoException(SiteAdminPage.customerTab);
			flags.add(JSClick(SiteAdminPage.customerTab,
					"Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Loading Image");

			//Verify the text in SaleforceID field is reverted to the Original value for that Customer
			flags.add(assertElementPresent(SiteAdminPage.salesForceIDInput,
					"SalesForce input text field."));
			flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput,
					originalID.trim(),"ID is reverted to the original value."));

			//Set flag to true indicating Success
			flag = true;

			LOG.info("verifySalesForceRevert function has completed.");
			if(flags.contains(false)){
				LOG.error(flags);
				throw new Exception();
			}
		}catch (Exception e){
			LOG.error("verifySalesForceRevert function has failed.");
			e.printStackTrace();
		}
		return flag;
	}
*/}
