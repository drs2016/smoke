package smoke.objects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;



import org.openqa.selenium.support.ui.Select;

public class TravelTrackerHomePage {

	/**
	 * Below Locators are related to Login information in the Travel Tracker
	 * Login page
	 */
	public static By userName;
	public static By password;
	public static By loginButton;
	public static By userNameEverBridge;
	public static By passwordEverBridge;
	public static By loginBtnEverBridge;
	public static By hydLocationMarker;
	public static By intermsearchresult;
	public static By searchResultChkbx;
	public static By searchResultTravCount;
	public static By searchResultTravCountForHotels;

	/**
	 * Below Locators are related to Send Message Screen in Map Home Page
	 */
	public static By travellerCount;
	public static By loadingImage;
	public static By messageIcon;
	public static By subject;
	public static By messageBody;
	public static By twoSMSResponse;
	public static By sendMessageButton;
	public static By travellerName;
	public static By emailText;
	public static By emailCheckbox;
	public static By phoneText;
	public static By phoneCheckbox;
	public static By messageLength;
	public static By closePopup;
	public static By hotelResult;

	/**
	 * Below Locators are related to Communication History Page
	 */
	public static By communicationHistory;
	public static By messageType;
	public static By searchBtn;
	public static By searchResults;
	public static By searchResultsSubject;
	public static By riskLayerDropDown;
	public static By TravelRisklayer;
	public static By medicalRisklayer;
	public static By medicalRisk;
	public static By travelUpdate;
	public static By logOff;
	public static By logOffEverBridge;
	public static By toolsLink;
	public static By intlSOSPortal;
	public static By searchButtonIsosPortal;
	public static By printButton;
	public static By exportToExcelBtn;
	public static By exportToZipBtn;
	public static By messageManagerLink;
	public static By helpLink;
	public static By userGuide;
	public static By feedbackLink;
	public static By userSettingsLink;
	public static By changePwdChallengeQstns;
	public static By updateEmailAddress;
	public static By updateButton;
	public static By welcomePage;
	public static By noOfPages;
	public static By myTripSelectCustomer;
	public static By intlSOSURL;
	public static By resetBtn;
	public static By filtersBtn;
	public static By location;
	public static By nowCheckBox;
	public static By last31Days;
	public static By next24Hours;
	public static By next1to7Days;
	public static By next8to31Days;
	public static By alertCount;
	public static By internationalCheckbox;
	public static By domesticCheckbox;
	public static By expatriateCheckbox;
	public static By asiaAndPacific;
	public static By homeCountry;
	public static By extremeTravelChkbox;
	public static By highTravelChkbox;
	public static By mediumTravelChkbox;
	public static By lowTravelChkbox;
	public static By insignificantTravelChkbox;
	public static By extremeMedicalChkbox;
	public static By highMedicalChkbox;
	public static By mediumMedicalChkbox;
	public static By lowMedicalChkbox;
	public static By searchDropdown;
	public static By searchBox;
	public static By goButton;
	public static By tileContent;
	public static By locationName;
	public static By countryName;
	public static By travellerPanel;
	public static By locationsTravellerCount;
	public static By exportPeoplePane;
	public static By messageBtn;
	public static By travellerNameSearch;
	public static By searchtextmidresult;
	public static By firsttravellerNamesList;

	public static By travellerDetailsHeader;
	public static By mapHomeTab;
	public static By yearDateRange;
	public static By yearValue;
	public static By monthDateRange;
	public static By monthValue;
	public static By dayDateRange;
	public static By fromCalendarIcon;
	public static By dateRange;
	public static By flightCount;
	public static By flightName;
	public static By filterCountry;
	public static By travellersList;
	public static By closeTravellersList;
	public static By travellerCheckbox;
	public static By sortByFilter;
	public static By sortByCountry;
	public static By sortByTravellerCount;
	public static By alertsTab;
	public static By mapIFrame;
	public static By messageIFrame;
	public static By alertTile;
	public static By readMoreLink;
	public static By alertPopupHeader;
	public static By alertPopupLocation;
	public static By alertPopupEventDate;
	public static By closeAlertPopup;
	public static By serachResultValidation;
	public static By HotelSearchResultValidation;
	public static By ItineraryResultValidation;

	/**
	 * Regression test cases
	 */
	public static By findTrains;
	public static By findHotels;
	public static By findResults;
	public static By closeBtnSearch;
	public static By buildingsTab;
	public static By addBuilding;
	public static By buildingName;
	public static By buildingAddress;
	public static By buildingCity;
	public static By buildingCountry;
	public static By buildingSave;
	public static By flyBtn;
	public static By aerialBtn;
	public static By editBuilding;
	public static By findBuilding;

	public static By searchIcon;
	public static By travelerName;
	public static By sendMessageAtTravellerDetails;
	public static By sendMessageConfirmationlabel;
	public static By itineraryResultExpandIcon;
	public static By mapPoints;
	public static By buildingsCheckbox;
	public static By buildingsIconOnMap;
	public static By buildingsResultPane;
	public static By buildingDetailsForm;
	public static By buildingOfficeImage;

	public static By checkInsCheckbox;
	public static By trainName;
	public static By countryGuide;
	public static By countryGuideHeader;
	public static By travellerFromSearch;
	public static By flightFromSearch;
	public static By hotelName;
	public static By buildingFromResult;

	public static By medicalRiskIcon;
	public static By travelRiskIcon;
	public static By countryGuideIcon;
	public static By countryCount;
	public static By presetSelect;

	public static By arrowDownFilter;
	public static By vipTraveller;
	public static By nonVipTraveller;
	public static By ticketed;
	public static By nonTicketed;
	public static By travellerNameInTravellerList;
	public static By vipStatusIndicator;
	public static By nonVipStatusIndicator;
	public static By datePresetList;

	public static By travellerNameDetails;
	public static By travellerEmailDetails;
	public static By travellerCountryDetails;

	public static By sortByTitle;
	public static By sortByDate;

	public static By vismoTab;
	public static By vismoNormalLabel;
	public static By alertItemsCount;
	public static By countryNameInAlertsList;

	public static By intlSOSResourcesTab;
	public static By findIntlSOSResources;
	public static By intlSOSResourceAddress;
	public static By intlSOSResourcePhone;
	public static By intlSOSResourceImage;
	public static By intlSOSCheckBox;
	public static By disabledTwoWaySMSResponseOptions;
	public static By dateFromGMTCommunication;
	public static By dateToGMTCommunication;
	public static By loadingSearchResult;
	public static By communicationSearchResultTable;
	public static By messageIDInResultTable;
	public static By messageResponseInResultTable;

	public static By editLink;
	public static By phoneNumber;
	public static By invalidMobNoErrorMsg;
	public static By emailCC;
	public static By countryCode;
	public static By refreshLabel;

	public static By alertCountriesCount;
	public static By expandViewLink;
	public static By dateTimeDetails;
	public static By departureDetails;

	public static By zoomCountry;
	public static By zoomCity;
	public static By zoomAirport;
	public static By checkInsTab;
	public static By refreshCheckIn;
	public static By checkInsSearchBox;
	public static By locationsTab;

	public static By selectAllCheckbox;
	public static By alertTravellerNames;
	public static By travellerNameInAlertsList;
	public static By deSelectAllCheckbox;
	public static By dynamicemailID;
	// Ever Bridge
	public static By organizationLink;
	public static By organizationItemList;
	public static By organizationValue;
	public static By contactsTabInEverBridge;
	public static By notificationsTabInEverBridge;
	public static By emptyContactsList;
	public static By messageInNotificationResult;
	public static By settingsInNotification;
	public static By deliveryDetailsTab;
	public static By responseColInDeliveryDetails;
	public static By contactsSubtabinContacts;
	public static By contactsResultTable;
	public static By FirstNameInContactsResultTable;
	public static By homeCountryByTraveler;
	public static By exportLink;
	public static By sendMessageLink;
	public static By sendMessageLinkInDetails;
	public static By expandLink;

	// ======//
	public static By homePageFirstDropdown;
	public static By homePageFirstSearchBox;
	public static By homePageFirstGoButton;
	public static By travellerExpandButton;
	public static By editTripTravellerProfile;
	public static By editPopupButtonClick;
	public static By justClick;
	public static By phoneNumberTextBox;
	public static By clickEditWindowSaveButon;
	public static By SavedMsginEditWindowPopup;
	public static By TriporPnrinEditWindowPopup;
	public static By editTrainDetailsClickBtn;
	public static By trainDepartureCity;
	public static By trainArrivalCity;
	public static By tripDetailsSaveButton;
	public static By tripTrainDetailsDepartureDate;
	public static By tripTrainDetailsArrivalDate;
	public static By tripMsgAfterenteringTrainDetails;
	public static By tripPopupCloseBtn;
	public static By getAllKnownTravel;
	public static By departurePlace;
	public static By arrivalPlace;
	public static By createNewTripBtn;

	public static By departureCitySelect;
	public static By arrivalCitySelect;
	public static By tripCalenderDateEnter;

	// =====//
	public static By Openjaw;
	public static By OpenjawDaysDiff;
	// =====//
	public static By filterRegion;

	// ===//
	public static By clickonCodeshareFlight;
	public static By codeShareFlightPresent;
	public static By codeSharePresent;
	public static By nonOperatingFlight;
	public static By operatingFlight;

	public static By sendMsgLinkSubject;
	public static By sendMsgLinkTextBody;
	public static By sendMsgLinkButton;

	public static By closePanelBtn;

	public static By clickonanyTraveller;

	public static By travelDetails;
	public static By expandedTravellerName;
	// ===//

	// ===//

	public static By searchFilterOptionforTravellers;
	public static By searchFilterOptionforTrains;
	public static By searchFilterOptionforFlights;
	public static By searchFilterOptionforHotels;
	public static By getSearchFilterPeopleCt;
	public static By searchApartFilterOption;
	public static By exportButton;
	public static By buildingNotes;
	public static By buildingContactName;
	public static By buildingContactEmailAdrs;
	public static By dateRangeDropdown;

	// ===//

	public static By editAccommodationDetails;
	public static By expatIndicationIcon;
	public static By updateLink;
	public static By firstTrip;

	public static By vismoImage;
	public static By homeCountrySearchResult;
	public static By replyTo;

	public static By ticketingStatusHeader;
	public static By buildingType;

	public static By selectAllInTravelerListHeader;
	public static By hotelInfoIntravelerListExpand;
	public static By hotelIconIntravelerListExpand;
	public static By hideTravelDetails;
	public static By showTravelDetails;

	public static By nameWithFirstAndLastName;
	public static By emailIdInExpandedTravellerList;
	public static By phoneNoInExpandedTravellerList;
	public static By homeCountryInExpandedTravellerList;
	public static By vipStatusInExpandedTravellerList;
	public static By expatInExpandedTravellerList;
	public static By moreTravellerDetailsInExpandedTravellerList;

	public static By manualTripLinkInExpandedTravellerList;
	public static By getAllKnownTravelInExpandedTravellerList;

	public static By additionalTripInfoPopup;
	public static By allKnownTravelsPopup;
	public static By asiaPacificTravellerCount;
	public static By travelerNameInExpandedView;

	public static By createdTravelerName;
	public static By profileDetails;
	public static By tripDetails;
	public static By travellerNamesList;
	public static By lasttravellerNamesList;

	public static By buildingsEnabledCheckbox;
	public static By siteAdminUpdate;
	public static By successUpdationMsg;

	public static By nameInTravelDetails;
	public static By departureLableInTravelDetails;
	public static By arrivalLableInTravelDetails;

	public static By mapUI;
	public static By buildingFilterOption;
	public static By addNewBuildingBtn;

	public static By enterNameinNewBuild;
	public static By enterAddressinNewBuild;
	public static By enterCityinNewBuild;
	public static By enterCountryinNewBuild;
	public static By saveBtnClickinNewBuild;
	public static By searchResultsNewBuild;
	public static By searchfromResults;
	public static By searchforMapPoint;

	public static By countOfIntrenationalInLocationsPane;

	public static By medicalExtremeCheckBox;
	public static By medicalHighCheckBox;
	public static By medicalMediumCheckBox;
	public static By medicalLowCheckBox;

	public static By twoWaySMSEnabledCheckbox;
	public static By homeCountryVisibility;

	public static By closeDetailPopUp;
	public static By clickProfileTips;

	public static By arrivalCityErrorMsg;
	public static By evacuationNotificationschkbx;
	public static By specialAdvisorieschkbx;
	public static By travelUpdateschkbx;
	public static By medicalAlerts;
	public static By travelAndBuildTrvelrRisks;

	public static By proActBtnEmailUpdate;
	public static By proactiveEmailsNotification;
	public static By manualEntrychkbx;
	public static By myTripsEntrychkbx;
	public static By mobileCheckinEntry;
	public static By tripSourceFilter1_btnUpdate;
	public static By tripSourceFilter1_lblNotification;

	public static By embassyCheckbox;
	public static By embassyIcon;
	public static By embassyNames;
	public static By assistanceCenterCheckbox;
	public static By clinicCheckbox;
	public static By internationalSOSHeader;

	// Sep 9
	public static By travelUpdateAlert;
	public static By medicalAlert;
	public static By specialAdvisory;

	public static By customerTab;
	public static By noTravellersFound;
	public static By vismoCustomer;

	// Sep 19
	public static By refineByTravellerTypeHeader;
	public static By refineByRiskHeader;
	public static By refineByTravelDateHeader;
	public static By refineByTravellerHomeCountryHeader;
	public static By additionalFiltersHeader;
	public static By extremeRatingLabel;
	public static By highRatingLabel;
	public static By mediumRatingLabel;
	public static By lowRatingLabel;
	public static By insignificantRatingLabel;
	public static By travelAlertImage;
	public static By specialAdvisoryCheckbox;
	public static By medicalAlertImage;
	public static By specialAdvisoryAlertImage;
	public static By specialAdvisoryAlert;

	// sep 20
	public static By codeShareIcon;
	public static By codeShareTravellerCount;

	public static By deleteBtn;
	public static By noBuildingmsg;
	public static By okBtn;

	public static By zoomIn;
	public static By medicalRiskRating;
	public static By travelRiskRating;
	public static By indiaCluster;
	public static By indiaInMap;
	public static By alertPoint;

	public static By officeBuildingType;
	public static By accommodationBuildingType;
	public static By otherBuildingType;
	public static By buildingAccommodationImage;
	public static By buildingOtherImage;
	public static By clinicImage;
	public static By embassyImage;

	public static By flightResult;
	public static By arrivalFlight;
	public static By departureFlight;

	public static By buildingSortBy;
	public static By buildingNameSortBy;
	public static By buildingTypeSortBy;
	public static By buildingCitySortBy;
	public static By buildingCountrySortBy;
	public static By buildingGeoCodeSortBy;
	public static By buildingStateSortBy;
	public static By btnFlyTo;

	public static By standardExportOption;
	public static By fullItineraryExportOption;
	public static By standardExportChkBox;
	public static By fullItineraryExportChkBox;
	public static By updateBtnExportOption;
	public static By updateMsgExportOption;

	public static By currentLocChkBox;

	public static By alertReadMore;
	public static By readMorePopup;
	public static By readMorePopupDtls;
	public static By readMorePopupDtls1;

	public static By alertCountryName;

	public static By buildingNameInList;
	public static By buildingNamesCount;

	public static By peopleCount;
	public static By homeCountryDisplayed;
	public static By homeCountriesList;

	public static By flightDetailsClick;

	public static By refineTraveller;
	public static By refineRisk;

	// Oct 19
	public static By characterCount;
	public static By addResponse;

	public static By travelDetail;
	public static By codeShareOption;
	public static By totalPeople;
	public static By airLineErrorMsg;
	public static By travellerErrorMsg;

	public static By toCalenderIcon;

	public static By selectsTravellerChkBox;
	public static By travellerListPane;

	public static By trainChkBox;
	public static By personArrowofTraveller;
	public static By lazyLoadingImage;

	public static By emailChkboxLabel;
	public static By smsChkboxLabel;
	public static By smsCheckbox;
	public static By textToSpeechChkboxLabel;
	public static By textToSpeechChkbox;
	public static By commsAddonCheckbox;

	public static By sortByInBuildingsTab;
	public static By buildingNameSortByInBuildingsTab;
	public static By buildingTypeSortByInBuildingsTab;
	public static By buildingCitySortByInBuildingsTab;
	public static By buildingCountrySortByInBuildingsTab;
	public static By buildingGeoCodeSortByInBuildingsTab;
	public static By buildingStateSortByInBuildingsTab;
	public static By btnFlyToInBuildingsTab;

	public static By sortByInResourcesTab;
	public static By buildingNameSortByInResourcesTab;
	public static By buildingTypeSortByInResourcesTab;
	public static By buildingCitySortByInResourcesTab;
	public static By buildingCountrySortByInResourcesTab;
	public static By buildingGeoCodeSortByInResourcesTab;
	public static By buildingStateSortByInResourcesTab;
	public static By btnFlyToInResourcesTab;

	public static By travelAlert;

	public static By uberTab;
	public static By uberChkBoxinMappoints;
	public static By uberPointsonMap;
	public static By showAllSegmentsDropDown;
	public static By TRAndTimerSymbol;

	public static By searchResult;

	public static By uberTraveller;
	public static By uberOption;
	public static By uberDuration;
	public static By uberDropOffs;
	public static By uberFirstTraveller;
	public static By uberNarrowViewOption;
	public static By ubernarrowViewDuration;

	public static By travellersListLinkInCheckInsTab;
	public static By checkInsOnlyinShowAllSegmentDropDown;
	public static By selectTravellerfromCheckINsList;
	public static By blueManImageinCheckIn;
	public static By travellerSearchInTravellersPane;

	public static By uberMesgIcon;
	public static By uberTravellerChkBoxDynamic;
	// public static By uberMsgWindowCloseBtn;
	public static By uberMsgWindowCloseBtn;
	public static By allMessagesHeaderinCommHistory;
	public static By showFiltersBtninCommHistory;
	public static By listofMessagesTableinCommHistory;
	public static By lastSubjectfromtheListofMessages;
	public static By frameinCommHistoryPage;
	public static By filterRecipientsBtninCommHistory;
	public static By resetFiltersBtninCommHistory;
	public static By firstNameinRecipientsListofCommHistory;
	public static By lastNameinRecipientsListofCommHistory;
	public static By phoneNumberinRecipientsListofCommHistory;
	public static By emailinRecipientsListofCommHistory;
	public static By closePopUPFrameinCommHistory;

	public static By applyFiltersinShowFiltersCommHistory;
	public static By msgCrntlyDisplayRecipientsinCommHistory;
	public static By frameinRecipientslistSendMessage;
	public static By sendanewmessageLabelinFrameinCommHistory;
	public static By closePopUpFrameofSendMessageinCommHistory;
	public static By applyFiltersinRecipientsPageCommHistory;
	public static By searchResultTravellerCountinPane;

	public static By uberTime;
	public static By uberSortOption;
	public static By uberSortDropOffTime;
	public static By uberSortFirstName;
	public static By uberSortLastName;
	public static By uberTravellers;
	public static By uberTimeCt;
	public static By uberTravellersCt;

	public static By labelName;
	public static By loadingText;

	public static By refreshVismo;
	public static By vismoSearchBox;

	public static By showTravellersFrominUserSettings;
	public static By availableGroupsinUserSettings;
	public static By selectedGroupsinUserSettings;
	public static By updateBtninUserSettings;

	public static By userSettings;
	public static By userSettingsUpdate;

	public static By buildingIconsOnMap;
	public static By availableGrpuserSettings;
	public static By rightArwinUserSettings;
	// DEc 19
	public static By travelerType;
	public static By vismoTypeInExpandedView;
	public static By assistanceAppTypeInExpandedView;
	public static By vismoCheckBox;
	public static By appUserLabelInExapndedView;
	public static By homeCountryDetails;
	public static By refreshUber;

	// Dec 26
	public static By dropOffInfo;
	public static By uberTravelerDateAndTime;
	public static By uberTravelerDate;
	public static By uberDropOffDesc;

	public static By addLabelText;
	public static By alertsImg;
	public static By alertsChkBox;
	public static By uberChkBox;
	public static By checkinsImg;
	public static By uberImg;
	public static By vismoImg;
	public static By riskLayers;
	public static By travelRiskRatingsOption;
	public static By medicalRiskRatingsOption;
	public static By showRiskRatingsCheckbox;
	public static By onlyShowRiskRatingsCheckbox;
	public static By IntlResourcesImg;
	public static By appUserLabelInNarrowView;

	// Jan 06
	public static By last48HoursText;
	public static By uberTravelerCount;
	public static By uberSortBy;
	public static By uberClosePanel;
	public static By uberSearchbox;
	public static By uberImageOnMap;
	public static By uberDropOffTooltipText;
	public static By uberHeaderText;

	public static By closeTravellerPanel;
	public static By locationCountryZoom;
	public static By peopleCtOnMap;
	public static By uberOptionAfterExpand;

	public static By countryNameTooltip;
	public static By peopleTooltip;
	public static By zoomInForAsiaAndPacific;
	public static By listOfCountries;
	public static By country;
	public static By road;
	public static By aerial;
	public static By dark;
	public static By isosLogo;
	public static By controlRisksLogo;
	public static By zoomInForAmericas;
	public static By zoomInLinkOnMap;
	public static By cityName;
	public static By cityTravelerCount;
	public static By xMarkOnLabelTextbox;

	public static By firstTravellerInAlerts;
	
	public static By uberList;
	public static By firstTravellerfromUberList;
	public static By travellerCountfromUber;
	public static By uberTimefromList;
	public static By lastTravellerfromUberList;

	public static By searchResultsListPane;
	public static By searchResultsfromList;

	public static By peopleCtForHotel;
	public static By peopleCtArrowImg;
	public static By hotelAddress;
	public static By nonFilterClick;
	public static By firstSearchResultsfromList;

	public static By arrivingDeparting;

	public static By checkInTraveller;
	public static By checkInMapPoint;

	public static By alertSecurityImage;
	public static By locationPanelHeader;
	public static By firstResultfromLocationPaneHeader;
	public static By peoplePanelHeader;
	public static By firstResultfromPeoplePaneHeader;
	public static By trainNameAndNum;
	public static By trainTravelerCount;
	public static By trainsCount;
	public static By trainNameInList;
	public static By trainDepartureDateAndTime;
	public static By trainArrivalDateAndTime;
	public static By noTrainFound;

	public static By toolTipbesideSendBy;
	public static By toolTipContent;
	public static By toolTipContentLink;

	public static By clickOnState;
	public static By getStatePeopleCt;
	public static By countOnMap;
	public static By diffTravellerCt;

	public static By travellerChkBox;
	public static By messageWindowCloseBn;
	public static By travellerEmaiID;
	public static By travellerPhoneNum;
	public static By moreTravellerLink;
	public static By getAllKnownTraveller;
	public static By getAllKnownTravellerWindow;

	public static By oneWayMessagelink;
	public static By forgotPasswordLink;
	public static By userNameinForgotPasswordPage;
	public static By submitBtninForgotPasswordPage;
	public static By securityAnswerField;
	public static By warningMessageInSecurityQuestionPage;

	public static By noHotelsFound;

	public static By locationLondon;
	public static By LocOnMapCt;
	public static By locTravellerTypeCt;
	public static By railLocTravellerTypeCt;
	public static By locationTilePplCt;
	public static By locationTileName;

	public static By viewOnMap;
	public static By viewOnMap1;
	public static By expatImg;
	public static By buildingsFlyIcon;

	public static By checkInTimeCt;
	public static By vismoTimeCt;
	public static By checkInTime;
	public static By vismoTime;

	public static By additionalFiltersinFiltersTab;
	public static By refineByTripSourceHeader;
	public static By travelAgentChkbox;

	public static By manualEntryChkbox;
	public static By myTrpsEntryChkbox;
	public static By myTripsFrwdMailsChkbox;
	public static By myTripsFrwdMailsCheckbox;
	public static By assistanceAppCheckInChkbox;
	public static By vismoCheckInChkbox;
	public static By uberDropOffChkbox;
	public static By selectAllChkBoxfromLocationPane;
	public static By exportBtninPeoplePane;

	public static By sendMsginPeoplePane;
	public static By travellersCtfromSendMsgScrn;

	public static By advanceFilters;
	public static By travelAgentCheckBox;
	public static By manualEntryCheckBox;
	public static By myTripsEntryCheckBox;
	public static By myTripsEmailsCheckBox;
	public static By asstAppCheckBox;
	public static By vismoCheckinCheckBox;
	public static By uberCheckBox;

	public static By travellerspane;
	public static By travellersCompletePanel;

	public static By userSettingsDisplay;
	public static By userSettingsUpdateUserInfoHeader;
	public static By userSettingsProactivemailsHeader;
	public static By userSettingsExcelExportHeader;
	public static By userSettingsChooseExportHeader;

	public static By manualEntry;
	public static By myTripsEntry;
	public static By asstApp;

	public static By selectAfricafrmTheList;

	public static By firsttravellerChkBox;
	public static By verifySubjectInCommPage;

	public static By clickMoreTrvlDtls;
	public static By clickLessTrvlDtls;
	public static By clickTrvlFromList;

	public static By msgDtlsMsgdisplay;
	public static By MsgRcptPopup;
	public static By verifySubjectInCommPageAndClk;

	public static By mapHomeCustDrpDwn;
	public static By ignoreTimeFilter;

	public static By vismoText;
	public static By vismoHomeCountry;
	public static By hotelImg;

	public static By hotelCount;
	public static By employeeId;
	public static By moreTravellerDetailsInTravelDetails;

	public static By narrowViewEmail;
	public static By expandedViewEmail;

	public static By narrowViewPhoneNum;
	public static By expandedViewPhoneNum;

	public static By choosePeopleExportColumns;
	public static By availableHeaderinChoosePeople;
	public static By selectedHeaderinChoosePeople;
	public static By updateBtninChoosePeople;

	public static By arrowDownFilterOfAdditional;
	public static By arrowDownFilterOfAdvanced;

	public static By userSettingsSelected;
	public static By userSettingsAvailable;
	public static By userSettingsRightArrow;
	public static By userSettingsLeftArrow;
	public static By userSettingsUpdateBtn;
	public static By userSettingsSuccessMsg;

	public static By userSettingsUpArrow;
	public static By userSettingsDownArrow;
	public static By morethan1ColumnSelectErrMsg;

	// Need To Checkin
	public static By chooseExpCol;
	public static By availableListBox;
	public static By selectedListBox;

	public static By travelUpdateCheckbox;
	public static By MedicalAlertCheckbox;
	public static By Noalertsfound;

	public static By selectedColumnsInUserSettings;
	public static By dynamicLocation;

	public static By MedicalAlertImageOnMap;
	public static By TravelUpdateImageOnMap;
	public static By specialAdvisoryAlertImageOnMap;
	public static By ZoomInOnMap;
	public static By travellerCheckboxInMapHomePage;
	public static By MedicalAlertOnMap;
	public static By expandedViewForSingleUserEmail;

	public static By intlSOSResourcesCountry;
	public static By refineByDataSource;
	public static By hotelNameInTravellerDetails;
	public static By hotelAddressInTravellerDetails;
	public static By hotelCheckInDateTravellerDetails;
	public static By hotelCheckOutDateTravellerDetails;
	public static By flightNameAndNumber;
	public static By flightDepartureDateInTravelerDetails;
	public static By flightArrivalDateInTravelerDetails;
	public static By dynamicLocationToSeeSublocation;
	public static By numberOfHotelsInTravelDetails;
	public static By travelerNameSearchResult;
	public static By getAllKnownTravelInNarrowView;
	public static By emailFirstName;
	public static By emailLastName;
	public static By editoptiononsendmsgpage;
	public static By alertTab;
	public static By searchwithinresult;
	public static By crossborderdetail;
	public static By crossbordercountrydetail;
	public static By sendNewMessageTitle;
	public static By travellerContactArrowExpanded;
	public static By travellerHeader;
	public static By travellerNameTab;
	public static By travellerAddRecipient;
	public static By travellerContactArrowCollapsed;
	public static By localContactArrowExpanded;
	public static By localContactArrowCollapsed;
	public static By includeLocalContacts;
	public static By hideFilterButtonText;
	public static By showFilterButtonText;

	public static By sendNewMsgCloseButton;

	public static By travelReadyText;
	public static By travelReadySandSymbol;

	public static By alertsTravellerCt;

	public static By removeLocalContacts;

	public static By alertsTravellerCtForIndia;
	public static By deleteTravellerfromMsgWindow;

	public static By statusNonVIP;
	public static By statusVIP;

	public static By flightDepartureAllKnownTravel;
	public static By flightNameAllKnownTravel;
	public static By trainDepartureAllKnownTravel;
	public static By flightDepartureCity;
	public static By flightArrivalCity;
	public static By hotelCheckinAllKnownTravel;
	public static By hotelNameAllKnownTravel;

	public static By oneWaySMS;
	public static By appNotificationChkBox;
	public static By twoWaySMSOption;
	public static By travellerFirstName;
	public static By CheckinText;
	public static By peopleCtForFlight;
	public static By filterCity;
	public static By firstcountry;
	public static By secondcountry;
	public static By searchfilter;
	public static By travelerstripsegments;

	public static By uberTravelerEndDate;
	public static By uberEnddateDetailrow;

	public static By uberDropoffDays;
	public static By uberEnddateDetailRowInExpanded;
	public static By uberTravellerEnddateinExpanded;
	public static By cities;
	public static By listOfCities;
	public static By radiobutton;
	public static By alerts;
	public static By searchTraveller;
	public static By replaceEmail;

	public static By clickonRegionCount;
	public static By disableincludelocalcontact;
	public static By noTravellerFoundError;
	public static By checkSpecialAdvisory;
	public static By travelAlertCheckbox;
	public static By checkMedicalAlert;
	public static By firstTravellerFromSearch;
	public static By localcontacttraveller;
	public static By trainSectionLabel;

	public static By unreachableTravellers;
	public static By emailID;
	public static By localcontactcollapse;
	public static By applyDefaultOrder;

	public static By exportColumnsMoveUpBtn;
	public static By exportColumnsMoveDownBtn;
	public static By multiOptionMoveError;
	public static By uberTravellerInUber;
	public static By vismoCheckinDateConv;

	public static By profileCategory;
	public static By stayCategory;
	public static By inboundCategory;
	public static By accommodationCategory;
	public static By carRentalCategory;
	public static By outboundCategory;

	public static By selectBasicColumns;

	public static By profileInSelected;
	public static By stayInSelected;
	public static By inBoundInSelected;
	public static By accomdationInSelected;
	public static By carRentalInSelected;
	public static By outBoundInSelected;

	public static By accommodationAdress;
	public static By accommodationPhone;
	public static By accommodationCheckInDate;
	public static By accommodationCheckOutDate;

	public static By locationCountClick;

	public static By basicColToolTip;
	public static By lastTraveller;
	
	public static By columnCategoryProfile;
	public static By columnCategoryStay;
	public static By columnCategoryInbound;
	public static By columnCategoryCarRental;
	public static By columnCategoryAccommodation;
	public static By columnCategoryOutbound;
	public static By firstnameRandom;
	public static By TrustelogoImage;
	
	public static By groupsuccessmsg;
	public static By selectedGroupOptions;
	public static By homeCountryInMap;
	public static By uberSortFirstNameAscArrow;
	public static By uberSortLastNameAscArrow;
	public static By peopleCountlabel;
	public static By personcountlabel;
	public static By travellerList;
	public static By peopleOrpersonLabel;
	public static By operationalFlight;
	public static By nonOperationalflight;
	public static By Operationalflightlist;
	public static By nonOperationalflightlist;
	public static By findResults1;
	public static By PNRticketingStatus;
	public static By TicketingStatus;
	public static By countrydetail;
	public static By ForAsiaAndPacificRegion;
	public static By noCheckinsFound;
	public static By noVismoFound;
	public static By vismoTravellersCt;
	public static By vismoTraveller;
	public static By vismoCheckinTime;
	public static By vismoFirstnamefield;
	public static By vismoLastirstnamefield;
	public static By checkInsSortOption;
	 public static By checkInsSortFirstName;
	 public static By checkInsSortLastName;
	 public static By checkInsSortCheckInTime;
	 public static By checkInsSortFirstNameAscArrow;
	 public static By checkInsSortLastNameAscArrow;
	 public static By checkInsSortCheckInTimeAscArrow;
	 
	 public static By checkInsTravellersCount;
	 public static By checkInsTravellers;
	 public static By checkInsTime;
	 
	 public static By departureCityErrorMsg;
	 public static By arrivalCityError;
	 public static By localContactCollapsedarrow;
	public void travelTrackerHomePage()
	{
		crossbordercountrydetail= By.xpath("(//*[@class='alertCountryName'])[last()]");
	
		departureCityErrorMsg = By.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport']/following-sibling::p");
		arrivalCityError = By.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_updatePanelSmartTextbox']//p");
		checkInsSortOption = By.xpath("//a[@class='my_sortbuttonlink']/img[@class='small']");
		 checkInsSortCheckInTime=By.xpath(".//*[@id='checkInsageInSecondsField']");
		  checkInsSortLastName=By.xpath(".//*[@id='checkInslastNameField']");
		  checkInsSortFirstName=By.xpath("//*[@id='checkInsfirstNameField']");

		  checkInsSortFirstNameAscArrow = By.xpath("//li[@id='checkInsfirstNameField']//img[contains(@src,'sort_asc')]");
		  checkInsSortLastNameAscArrow = By.xpath("//li[@id='checkInslastNameField']//img[contains(@src,'sort_asc')]");
		  checkInsSortCheckInTimeAscArrow = By.xpath("//li[@id='checkInsageInSecondsField']//img[contains(@src,'sort_asc')]");

		  checkInsTravellersCount= By.xpath("//div[@class='block_box']//h1");
		  
		  checkInsTravellers = By.xpath("//ul[@id='checkins-list-container']//li['<replaceValue>']//div[@class='block_box']//h1");
		  
		  checkInsTime = By
		    .xpath("//ul[@id='checkins-list-container']//li['<replaceValue>']//p[@class='black-link']");
		
		  firstTravellerInAlerts= By.xpath("//div[@id='alertId_0']//h2");
		  
		vismoTravellersCt = By.xpath("//div[@class='block_box']//h1");
		vismoLastirstnamefield=By.xpath(".//*[@id='vismoCheckInslastNameField']");
		vismoCheckinTime=By.xpath(".//li[@id='vismoCheckInsageInSecondsField']");
	    vismoFirstnamefield=By.xpath(".//*[@id='vismoCheckInsfirstNameField']");
	    vismoTraveller=By.xpath("//ul[@id='vismo-checkins-list-container']//li[1]//div[@class='block_box']//h1");
		noCheckinsFound = By.xpath("//div[@id='tileContent']//div[@class='filter_lists clearfix']/div[contains(text(),'No check-ins found')]");
		noVismoFound = By.xpath("//div[@id='tileContent']//div[@class='filter_lists clearfix']/div[contains(text(),'No vismo found')]");
		ForAsiaAndPacificRegion = By.xpath("//h1[text()='Asia & the Pacific']");
		countrydetail = By.xpath("(//div[contains(@id,'location')]/div/div[@class='clearfix'])[last()]");
		TicketingStatus = By.xpath("//div[@id='container']/span[contains(text(),'Ticketing')]");
		PNRticketingStatus = By.xpath("(//*[@id='117189']/a[contains(@class,'PNRstatus')])[last()]");
		peopleCountlabel=By.xpath("(//label[contains(text(),'Person')])[last()]");
		personcountlabel=By.xpath("(//label[contains(text(),'People')])[last()]");	
		operationalFlight=By.xpath("//div[@class='fltLeft travellers']/div/span");
		nonOperationalflight=By.xpath("//div[@class='fltLeft travellers']/div/span");
		nonOperationalflightlist=By.xpath("//div[@class='search-scroll-area-flight']/ul/li/div");
		Operationalflightlist=By.xpath("//div[@class='search-scroll-area-flight']/ul/li/div");
		peopleOrpersonLabel=By.xpath("(//label[contains(text(),'People') or contains(text(),'Person')])[last()]");
		travellerList=By.xpath("//*[@id='scrollDiv']");
		selectedGroupOptions = By.xpath("//Select[@id='ctl00_MainContent_AssignGroupToUser1_lstSelectedGroup']/option");
		groupsuccessmsg = By.id("ctl00_MainContent_AssignGroupToUser1_lblMessage");
		firstnameRandom = By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']//option[contains(text(),'<replaceValue>')]");
		columnCategoryProfile=By.xpath("//optgroup[@label='Profile']/option[contains(text(),'<replaceValue>')]");
		columnCategoryStay=By.xpath("//optgroup[@label='Stay']/option[contains(text(),'<replaceValue>')]");
		columnCategoryInbound=By.xpath("//optgroup[@label='Inbound']/option[contains(text(),'<replaceValue>')]");
		columnCategoryCarRental=By.xpath("//optgroup[@label='Car Rental']/option[contains(text(),'<replaceValue>')]");
		columnCategoryAccommodation=By.xpath("//optgroup[@label='Accommodation']/option[contains(text(),'<replaceValue>')]");
		columnCategoryOutbound=By.xpath("//optgroup[@label='Outbound']/option[contains(text(),'<replaceValue>')]");
		
		lastTraveller = By
				.xpath("(//div[@id='scrollDiv']//h2//preceding-sibling::div//input//following-sibling::label)[last()]");
		basicColToolTip = By.id("ctl00_MainContent_ExportOptions_imgQueIcon");
		userSettingsUpdateUserInfoHeader = By
				.xpath("//span[contains(text(),'Update User Information')]");
		userSettingsProactivemailsHeader = By
				.xpath("//span[contains(text(),'Proactive Emails')]");
		userSettingsExcelExportHeader = By
				.xpath("//span[contains(text(),'Excel Export Options')]");
		userSettingsChooseExportHeader = By
				.xpath("//span[contains(text(),'Choose Export Columns')]");
		locationCountClick = By
				.xpath("(//div[@class='locationPeopleDiv']//h3[contains(text(),'People')])[last()]");

		selectBasicColumns = By
				.id("ctl00_MainContent_ExportOptions_btnSetSelectedToBasic");
		accommodationAdress = By
				.xpath("//option[contains(text(),'Accommodation Address')]");
		accommodationPhone = By
				.xpath("//option[contains(text(),'Accommodation Phone')]");
		accommodationCheckInDate = By
				.xpath("//option[contains(text(),'Accommodation Check-in date')]");
		accommodationCheckOutDate = By
				.xpath("//option[contains(text(),'Accommodation Check-out date')]");

		profileInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Profile']");
		stayInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Stay']");
		inBoundInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Inbound']");
		accomdationInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Accommodation']");
		carRentalInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Car Rental']");
		outBoundInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Outbound']");

		profileCategory = By.xpath("//optgroup[@label='Profile']");
		stayCategory = By.xpath("//optgroup[@label='Stay']");
		inboundCategory = By.xpath("//optgroup[@label='Inbound']");
		accommodationCategory = By.xpath("//optgroup[@label='Accommodation']");
		carRentalCategory = By.xpath("//optgroup[@label='Car Rental']");
		outboundCategory = By.xpath("//optgroup[@label='Outbound']");

		clickonRegionCount = By
				.xpath("//h1[text()='<replaceValue>']/../../following-sibling::div//div/h2");

		replaceEmail = By.xpath("//a[contains(@href,'<replaceValue>')]");
		sendNewMsgCloseButton = By
				.xpath("//div[contains(@class,'msgPopupComman')]//a[@title='Close Panel']");
		showFilterButtonText = By.xpath("//input[@value='Show filters']");
		hideFilterButtonText = By.xpath("//input[@value='Hide filters']");
		getAllKnownTravelInNarrowView = By
				.xpath(".//h6[@id='all-tvl-btn' and text()='Get All Known Travel']");
		travelerNameSearchResult = By
				.xpath("//span[contains(text(),'<replaceValue>')]");
		numberOfHotelsInTravelDetails = By
				.xpath("//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel']");
		dynamicLocationToSeeSublocation = By
				.xpath("//h1[text()='<replaceValue>']");
		flightArrivalDateInTravelerDetails = By
				.xpath("//div[@id='allKnownTravels']//div[@class='flightDetail']//span[contains(text(),'Arrival')]");
		flightDepartureDateInTravelerDetails = By
				.xpath("//div[@id='allKnownTravels']//div[@class='flightDetail']//span[contains(text(),'Departure')]");
		flightNameAndNumber = By
				.xpath("(//div[@id='allKnownTravels']//div[@id='segment-list'])[last()]//div[contains(@class,'flightList')]//span[contains(@class,'supplier-name')]");
		hotelCheckOutDateTravellerDetails = By
				.xpath("//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']/ancestor::p//following-sibling::div//div[text()='Check-Out']/following-sibling::div");

		hotelCheckInDateTravellerDetails = By
				.xpath("//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']/ancestor::p//following-sibling::div//div[text()='Check-In']/following-sibling::div");
		hotelAddressInTravellerDetails = By
				.xpath("//div[@id='allKnownTravels']//span[contains(@class,'right-margin') and text()='<replaceValue>']/ancestor::p/span[last()]");
		// span[contains(@class,'right-margin') and
		// text()='<replaceValue>']/ancestor::p/span[contains(@class,'font11')]
		hotelNameInTravellerDetails = By
				.xpath("//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']");
		// =====//
		commsAddonCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkCommsAddOn");
		hotelResult = By
				.xpath("(//div[@class='fltLeft travellers item-cont'])[last()]");
		homePageFirstDropdown = By.xpath("//select[@id='searchoptions']");
		homePageFirstSearchBox = By.xpath("//input[@name='deskSearch']");
		homePageFirstGoButton = By.xpath("//button[@id='btnSearch']");
		travellerExpandButton = By.xpath("//*[@id='expandViewLink']/a/img");
		editTripTravellerProfile = By
				.xpath("(//a[contains(text(),'Edit Trip/Traveller Profile')])[last()]");
		// editPopupButtonClick =
		// By.xpath("//a[@id='ctl00_MainContent_lnkEdit']");
		editPopupButtonClick = By.id("ctl00_MainContent_lnkEdit");
		justClick = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName']");
		searchtextmidresult = By.cssSelector(".search-text");
		phoneNumberTextBox = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_txtPhoneNo']");
		clickEditWindowSaveButon = By
				.xpath("//*[@id='ctl00_MainContent_btnUpdate']");
		SavedMsginEditWindowPopup = By
				.xpath("//span[@id='ctl00_MainContent_ucCreateProfile_lblMessage']");
		// TriporPnrinEditWindowPopup =
		// By.xpath("//a[text()='Test_June 10th']");
		TriporPnrinEditWindowPopup = By.xpath("//a[text()='<replaceValue>']");
		editTrainDetailsClickBtn = By
				.xpath("//img[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_EditSegmentTrain']");
		trainDepartureCity = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_departureCity_txtSmartTextBoxRailStation']");
		trainArrivalCity = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_arrivalCity_txtSmartTextBoxRailStation']");
		tripDetailsSaveButton = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_btnSaveTrain']");
		tripTrainDetailsDepartureDate = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtDepartureDate_txtDate']");
		tripTrainDetailsArrivalDate = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtArrivalDate_txtDate']");
		tripCalenderDateEnter = By
				.xpath("(//td[text()='<replaceValue>'])[last()]");
		tripMsgAfterenteringTrainDetails = By
				.xpath("//span[@id='ctl00_MainContent_ucCreateTrip_lblSuccessMsg']");
		tripPopupCloseBtn = By.id("close-popup");
		getAllKnownTravel = By.xpath("(//a[@id='all-tvl-btn'])[last()]");
		departurePlace = By
				.xpath("(//*[@id='segment-list']/div//div[contains(text(),'Tees')])[last()]");
		arrivalPlace = By
				.xpath("(//*[@id='segment-list']/div//div[contains(text(),'London')])[last()]");
		createNewTripBtn = By
				.xpath("//a[@id='ctl00_MiddleBarControl_linkCreateNewTrip']");

		departureCitySelect = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_departureCity_autocompleteDropDownPanel')]/div[1]");
		arrivalCitySelect = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_arrivalCity_autocompleteDropDownPanel')]/div[1]");

		filterRegion = By
			    .xpath("//div[@class='container search-container']//span[contains(text(),'[REGION] <replaceValue>')]");
		
		Openjaw = By
				.xpath("(//span[contains(@class,'code_openjaw float-right')])[last()]");
		OpenjawDaysDiff = By
				.xpath("(//span[contains(@class,'code_openjaw float-right')])[last()]/../../span[@class='myDateArriveDepart']/span[@class='padR15']/following-sibling::span");
		// ====//

		clickonCodeshareFlight = By
				.xpath("//span[@class='code_share float-right']");
		// codeShareFlightPresent = By.xpath("(//span[@class='supplier-name
		// padR15 blackText'])[last()]");
		codeShareFlightPresent = By
				.cssSelector(".actionClass.expandviewPanel.p-LR-5.clearfix >div:first-child > div:last-child >div:last-child p .supplier-name.padR15.blackText");
		codeSharePresent = By
				.xpath("(//span[@class='code_share float-right'])[last()]");
		nonOperatingFlight = By
				.xpath("(//span[@class='supplier-name padR15 blackText'])[last()]");
		// operatingFlight = By.xpath("((//span[@class='supplier-name padR15
		// blackText'])/span/following-sibling::span)[last()]");
		operatingFlight = By
				.cssSelector(".actionClass.expandviewPanel.p-LR-5.clearfix >div:first-child > div:last-child >div:last-child p .supplier-name.padR15.blackText>span:last-child");

		travellerCount = By
				.xpath("//div[@id='tileContent']//following-sibling::ul//li[1]//h2");
		loadingImage = By.xpath("//div[@id='tt-loading-overlay']");
		messageIcon = By.xpath("//div[@id='messageLink1']/a/img");
		subject = By.xpath("//input[@id='msgsubject']");
		messageBody = By.xpath("//textarea[@id='msgbody']");
		twoSMSResponse = By.xpath("//a[text()='Two-way message']");
		sendMessageButton = By.xpath("//a[text()='Send']");
		emailChkboxLabel = By.xpath("//label[contains(text(),'Email')]");
		emailCheckbox = By.xpath("//input[@id='chkemail']");
		smsChkboxLabel = By.xpath("//label[contains(text(),'SMS')]");
		smsCheckbox = By.xpath("//input[@id='chksms']");
		textToSpeechChkboxLabel = By
				.xpath("//label[contains(text(),'Text-to-Speech')]");
		textToSpeechChkbox = By.xpath("//input[@id='chktxt2v']");
		messageLength = By.xpath("//span[@id='charCnt']");
		// closePopup = By.cssSelector(".close-comm-panel a");

		closePopup = By.xpath("//div[@id='close-popup']");
		communicationHistory = By.xpath("//a[text()='Communication History']");
		messageType = By
				.xpath("//select[@id='ctl00_MainContent_drpMessageTypeId']");
		searchBtn = By.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		searchResults = By.xpath("//table[@id='ctl00_MainContent_gvResults']");
		searchResultsSubject = By
				.xpath("//table[@id='ctl00_MainContent_gvResults']//a[@id='ctl00_MainContent_gvResults_ctl02_lnkMessageID']");
		logOff = By.xpath("//li[@id='LogOff']");
		logOffEverBridge = By.xpath("//a[@href='/logout']");
		userName = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
		password = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_txtPassword']");
		loginButton = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_btnLogIn']");
		userNameEverBridge = By.id("username");
		passwordEverBridge = By.id("password");
		loginBtnEverBridge = By
				.xpath("//a[@id='proceed']/span[text()='Sign-in']");
		toolsLink = By.xpath("//a[contains(@id,'lnkTools')]");
		intlSOSPortal = By.xpath("//li[@id='ctl00_lnkIntlSOSPortal']");
		searchButtonIsosPortal = By
				.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		printButton = By.xpath("//input[@id='ctl00_MainContent_btnPrint']");
		exportToExcelBtn = By
				.xpath("//input[@id='ctl00_MainContent_btnExport']");
		exportToZipBtn = By.xpath("//*[@id='ctl00_MainContent_btnZipExport']");
		messageManagerLink = By.xpath("//li[@id='ctl00_lnkMessageManager']");
		helpLink = By.xpath("//li[@id='Help']");
		userGuide = By.xpath("//div[contains(text(),'Users Guide')]");
		feedbackLink = By.xpath("//li[@id='Feedback']");
		// userSettingsLink =
		// By.cssSelector("#ctl00_ContentPlaceHolder1_AdminHeaderControl_lnkUserSettings>a");
		// userSettingsLink = By.cssSelector("#ctl00_lnkUserSettings>a");
		userSettingsLink = By
				.xpath("//a[@href='UserSettings/UserSettings.aspx']");
		changePwdChallengeQstns = By
				.xpath("//a[@id='ctl00_MainContent_hlUpdatePassword']");
		updateEmailAddress = By
				.xpath("//a[@id='ctl00_MainContent_hlUpdateEmail']");
		updateButton = By
				.xpath("//input[@id='ctl00_MainContent_ProactiveEmails1_btnUpdate']");
		welcomePage = By.xpath("//span[contains(text(),'Welcome')]");
		noOfPages = By.xpath("//*[@id='numPages']");
		serachResultValidation = By
				.xpath(".//*[@id='tileContent']//ul/li[1]//label[contains(text(),'<replaceValue>')]");
		HotelSearchResultValidation = By
				.xpath(".//*[@id='tileContent']//ul/li[1]//div[contains(text(),'<replaceValue>')]");

		searchResult = By
				.xpath("//div[@id='tileContent']//ul/li//label[contains(text(),'<replaceValue>')]");
		ItineraryResultValidation = By
				.xpath("//div[@id='scrollDiv']/table/tbody/tr//h2/label");
		myTripSelectCustomer = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers']");
		intlSOSURL = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lblEncryptedValue']");
		resetBtn = By.xpath("//label[text()='Reset Map']");
		// filtersBtn = By.xpath("//div[@id='FilterButton']");
		filtersBtn = By.id("FilterButton");
		location = By.xpath("//select[@id='presetSelect']");
		/*
		 * last31Days = By.xpath("//input[@id='last31']"); nowCheckBox =
		 * By.xpath("//input[@id='Now']"); next24Hours =
		 * By.xpath("//input[@id='Next24h']"); next1to7Days =
		 * By.xpath("//label[@for='Next1to7d']"); next8to31Days =
		 * By.xpath("//input[@id='Next31d']");
		 */
		last31Days = By.xpath("//input[@id='last31']");
		//last31Days = By.xpath("//label [@for='last31']");
		nowCheckBox = By.xpath("//input[@id='Now']");
		next24Hours = By.xpath("//input[@id='Next24h']");
		next1to7Days = By.xpath("//label[@for='Next1to7d']");
		next8to31Days = By.xpath("//label[@for='Next31d']");
		alertCount = By.xpath("//h2[@title='Click to view details']");
		internationalCheckbox = By.xpath("//label[@for='international1']");
		domesticCheckbox = By.xpath("//label[@for='domestic1']");
		expatriateCheckbox = By.xpath("//label[@for='expatriate1']");
		homeCountry = By.xpath("//input[@id='country']");
		extremeTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='extreme']");
		highTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='high']");
		mediumTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='medium']");
		lowTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='low']");
		insignificantTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='insignificant']");
		extremeMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='extreme1']");
		highMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='high1']");
		mediumMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='medium1']");
		lowMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='low1']");
		searchDropdown = By.xpath("//select[@id='searchoptions']");
		searchBox = By.xpath("//input[@name='deskSearch']");

		dynamicemailID = By
				.xpath("(//a[contains(@href,'rekha.tiruvuru@gallop.net')]/../preceding-sibling::div/h2/span)[last()]");

		goButton = By.xpath("//button[@id='btnSearch']");
		tileContent = By.xpath("//div[@id='tileContent']");
		countryName = By
				.xpath("//div[@id='tileContent']//span[contains(text(),'Asia & the Pacific')]");

		exportPeoplePane = By.xpath("//div[contains(@id,'exportLink')]/a/img");
		messageBtn = By.cssSelector("#messageLink1");
		travellerDetailsHeader = By.xpath("//span[text()='Traveller Details']");
		mapHomeTab = By.xpath("//a[text()='Map Home']");
		yearDateRange = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']");
		yearValue = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']/option[text()='2009']");
		monthDateRange = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']");
		monthValue = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']/option[text()='May']");
		// dayDateRange = By.xpath("//div[@class='pika-single
		// is-bound']//table//button[text()='8']");
		dayDateRange = By
				.xpath("//div[@class='pika-single is-bound']//table//button[text()='<replaceValue>']");
		fromCalendarIcon = By.xpath("//li[@id='fromCalendarIcon']");
		dateRange = By.xpath("//li[text()='Date Range']");
		flightCount = By
				.xpath("//div[@id='tileContent']//div[contains(text(),'Flight')]");
		travellersList = By.xpath("//div[@id='travelerList']");
		closeTravellersList = By.xpath("//div[@id='closeTravelerDiv']//img");
		travellerCheckbox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		sortByFilter = By.xpath("//img[@title='Sort By']");
		sortByCountry = By.xpath("//li[@id='alertsLocationNameField']");
		sortByTravellerCount = By.xpath("//li[@id='alertsTravelerCountField']");
		alertsTab = By.xpath("//div[@id='alertsTab']");
		alertTile = By.xpath("//ul[@id='alertItems']/li[1]//h2");
		readMoreLink = By
				.xpath("//ul[@id='alertItems']/li[1]//span[text()='Read more']");
		alertPopupHeader = By.xpath("//div[@id='container']//h1");
		alertPopupLocation = By.xpath("//i[text()='Location']");
		alertPopupEventDate = By.xpath("//i[text()='Event Date']");
		closeAlertPopup = By.xpath("//div[@id='close-popup']");
		mapIFrame = By.cssSelector("#ctl00_MainContent_mapiframe");
		messageIFrame = By.id("messageIframe");
		/**
		 * Regression test cases
		 */
		findTrains = By.xpath("//input[@id='trainsFind']");
		findResults = By.xpath("//div[@id='tileContent']//li");
		findResults1=By.xpath("//div[@class='locationsListing alerts-height']/ul/li");
		findHotels = By.xpath("//input[@id='hotelsFind']");
		closeBtnSearch = By.xpath("//div[@id='closeBtnSearch']");
		buildingsTab = By.xpath("//div[@id='buildingsTab']");
		addBuilding = By.xpath("//button[@id='addBuilding']");
		buildingName = By.xpath("//input[@id='bname']");
		buildingAddress = By.xpath("//input[@id='baddress']");
		buildingCity = By.xpath("//input[@id='bcity']");
		buildingCountry = By.xpath("//input[@id='bcountry']");
		buildingSave = By.xpath("//button[@id='btnSubmit']");
		flyBtn = By.cssSelector("#btnFly");
		aerialBtn = By.xpath("//button[text()='Aerial']");
		editBuilding = By.xpath("//button[@id='btnEdit']");
		findBuilding = By.xpath("//input[@id='buildingsFind']");
		buildingNotes = By.id("bnotes");
		buildingContactName = By.id("contName");
		buildingContactEmailAdrs = By.id("contEmail");
		searchIcon = By.xpath("//img[@src='images/search-icon_white.png']");
		travelerName = By.xpath("//div[@id='scrollDiv']//li[1]//h2/span");
		sendMessageAtTravellerDetails = By.xpath("//a[@id='messageLink1']/img");
		sendMessageConfirmationlabel = By
				.xpath("//div[@id='modal']/div//div[@class='msg-send-notification']");
		itineraryResultExpandIcon = By
				.xpath("//img[@src='images/colapse-icon-sm.png']");

		mapPoints = By.xpath("//a[text()='Map Points']");
		buildingsCheckbox = By.xpath("//input[@id='chkBuildings']");
		buildingsIconOnMap = By
				.xpath("//span[@class='clusterMarkerIcons buildingClusterIcon']");
		buildingIconsOnMap = By.xpath("(//div[@id='map']//div[@class='leaflet-marker-pane']/img)[last()]");
		buildingsResultPane = By.xpath("//span[contains(text(),'Buildings')]");
		buildingDetailsForm = By.xpath("//form[@id='buildingDetailsForm']");
		buildingOfficeImage = By
				.xpath("(//img[contains(@src,'building_office')])[last()]");
		checkInsCheckbox = By.id("chkCheckins");
		firsttravellerNamesList = By
				.xpath("(//div[@class='travLazyloadingLoading']/../ul/li)[1]");

		filterCountry = By
				.xpath("//div[@class='container search-container']//span[contains(text(),'[COUNTRY] <replaceValue>')]");
		trainName = By.xpath(".//label[contains(text(),'<replaceValue>')]");
		countryGuide = By
				.xpath("//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryGuideHeader = By
				.xpath("//h1[contains(text(),'<replaceValue>')]");
		travellerFromSearch = By
				.xpath("(//*[@id='scrollDiv']//span[contains(text(),'<replaceValue>')])[last()]");
		firstTravellerFromSearch = By
				.xpath("(//*[@id='scrollDiv']//span[contains(text(),'<replaceValue>')])[1]");
		flightFromSearch = By
				.xpath("//label[contains(text(),'<replaceValue>')]");
		/*
		 * hotelName = By .xpath(
		 * "//*[@id='tileContent']//ul/li[1]/div/div[text()='<replaceValue>']");
		 */
		hotelName = By
				.xpath("(//div[contains(text(),'<replaceValue>')])[last()]");
		buildingFromResult = By.xpath("//h1[text()='<replaceValue>']");
		medicalRiskIcon = By
				.xpath("//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/medical_small_location_icon_n.png']");
		travelRiskIcon = By
				.xpath("//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/travel_small_location_icon_n.png']");
		countryGuideIcon = By
				.xpath("//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryCount = By
				.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h2");
		presetSelect = By.xpath("//select[@id='presetSelect']");

		arrowDownFilter = By.className("arrow-down_filter");
		arrowDownFilterOfAdditional = By
				.xpath("//*[@id='additionalFilterToggle']/span[@class='arrow-down_filter']");
		arrowDownFilterOfAdvanced = By
				.xpath("//*[@id='advanceFilterToggle']/span[@class='arrow-down_filter']");

		vipTraveller = By.id("vip");
		nonVipTraveller = By.id("nonVip");
		ticketed = By.id("ticketed");
		nonTicketed = By.id("unticketed");

		travellerNameInTravellerList = By
				.xpath(".//div[@id='scrollDiv']//li[1]//h2/span");
		vipStatusIndicator = By
				.xpath("//span[contains(text(),'VIP Status:Yes')]");
		nonVipStatusIndicator = By
				.xpath("//span[contains(text(),'VIP Status: No')]");
		datePresetList = By.xpath("//li[@title='Date Presets']");

		travellerNameDetails = By
				.xpath("//div[@id='scrollDiv']//li[1]/div/div//h2/span");
		travellerCountryDetails = By
				.xpath("//div[@id='scrollDiv']/ul/li[1]/div/div/span");
		travellerEmailDetails = By
				.xpath("//div[@id='scrollDiv']/ul/li[1]/div/div//a");

		sortByTitle = By.xpath("//li[@id='alertsTitleField']");
		sortByDate = By.xpath("//li[@id='alertsDateField']");

		vismoTab = By.id("vismoCheckInsTab");
		vismoNormalLabel = By.id("refeshVismoList");
		alertItemsCount = By.xpath("//ul[@id='alertItems']/li");
		countryNameInAlertsList = By
				.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h1");

		intlSOSResourcesTab = By.id("resourcesTab");
		findIntlSOSResources = By.id("resourcesFind");
		intlSOSResourceAddress = By.xpath("//label[text()='Address']");
		intlSOSResourcePhone = By.xpath("//label[text()='Contact Phone']");
		intlSOSCheckBox = By.id("chkResources");
		intlSOSResourceImage = By
				.xpath("(//img[contains(@src,'resources_ass')])[last()]");

		disabledTwoWaySMSResponseOptions = By
				.xpath("//input[@id='ctl00_MainContent_chkResponseOptions' and @disabled='disabled']");
		dateFromGMTCommunication = By.id("ctl00_MainContent_txtFromDate");
		dateToGMTCommunication = By.id("ctl00_MainContent_txtToDate");
		loadingSearchResult = By.xpath("//img[@src='../Images/loading.gif']");
		communicationSearchResultTable = By.id("ctl00_MainContent_gvResults");
		messageIDInResultTable = By
				.xpath("//a[@id='ctl00_MainContent_gvResults_ctl<replaceValue>_lnkMessageID']");
		messageResponseInResultTable = By
				.xpath("//span[@id='ctl00_MainContent_gvResults_ctl<replaceValue>_lblResponses']");

		editLink = By.xpath("//a[contains(@href,'Edit$0')]");
		phoneNumber = By.id("ctl00_MainContent_gvRecipients_ctl02_txtPhone");
		invalidMobNoErrorMsg = By.xpath("");
		emailCC = By.id("ctl00_MainContent_txtCC");
		countryCode = By
				.id("ctl00_MainContent_gvRecipients_ctl02_drpCountryCode");
		refreshLabel = By.id("refeshVismoList");
		alertCountriesCount = By.xpath("//ul[@id='alertItems']/li//h1");
		expandViewLink = By.xpath("//div[@id='expandViewLink']/a/img");
		dateTimeDetails = By
				.xpath("//div[contains(@id,'_0')]/div/p//span[contains(text(),':')]");
		departureDetails = By
				.xpath("//div[contains(@id,'_0')]//div[@id='segment-list']/div");

		zoomCountry = By.xpath("//span[text()='<replaceValue>']");
		checkInsTab = By.id("checkInsTab");
		refreshCheckIn = By.id("refeshCheckinList");
		checkInsSearchBox = By.id("checkInsFind");
		locationsTab = By.id("locationsTab");

		selectAllCheckbox = By.id("showallAlerts");
		alertTravellerNames = By.xpath("//div[@id='scrollDiv']//li//h2/span");
		travellerNameInAlertsList = By
				.xpath("//div[@id='scrollDiv']//li[<replaceValue>]//h2/span");
		deSelectAllCheckbox = By.xpath("//label[text()='Deselect All']");

		// Ever Bridge
		organizationLink = By.id("dropdown_menu");
		organizationItemList = By.id("itemsListview3");
		organizationValue = By.xpath("//a[text()='<replaceValue>']");
		contactsTabInEverBridge = By.xpath("//div[text()='Contacts']");
		contactsSubtabinContacts = By
				.xpath("//a[@id='ui-tabs-1' and text()='Contacts']");
		notificationsTabInEverBridge = By
				.xpath("//div[text()='Notifications']");
		emptyContactsList = By
				.xpath("//div[text()='There are no items to display in this table.']");
		messageInNotificationResult = By
				.xpath("//a[contains(text(),'<replaceValue>')]");
		settingsInNotification = By
				.xpath("//a[@id='ui-tabs-3' and contains(text(),'Settings')]");
		deliveryDetailsTab = By
				.xpath("//a[@id='ui-tabs-1' and contains(text(),'Delivery Details')]");
		responseColInDeliveryDetails = By.xpath("//div[text()='Response']");
		contactsResultTable = By.id("contacts_gridTable");
		FirstNameInContactsResultTable = By
				.xpath("//a[contains(text(),'<replaceValue>')]");

		exportLink = By.id("exportLink");
		sendMessageLink = By.cssSelector("#messageLink1");
		sendMessageLinkInDetails = By.cssSelector("div#messageLink1");
		expandLink = By.id("expandViewLink");

		editAccommodationDetails = By
				.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_EditSegmentAcco");
		expatIndicationIcon = By.xpath("//div[contains(@class,'expatIcon')]");
		updateLink = By.xpath("//a[text()='Update']");
		firstTrip = By
				.id("ctl00_MainContent_ucTripList_gvTripDetail_ctl02_linkBtnTripNameOrPnr");
		homeCountryByTraveler = By
				.xpath("//div[@id='scrollDiv']/ul//h2/span[contains(text(),'<replaceValue>'))]/../../following-sibling::span");
		// ====//

		searchFilterOptionforTravellers = By
				.xpath("//a[@id='searchTravellerNonFilter']");
		searchFilterOptionforTrains = By.xpath(".//*[@id='trainsNonFilter']");
		searchFilterOptionforHotels = By.xpath(".//*[@id='hotelsNonFilter']");
		searchFilterOptionforFlights = By.xpath(".//*[@id='flightsNonFilter']");

		getSearchFilterPeopleCt = By
				.xpath("//div[@class='traveler-count-big']");
		searchApartFilterOption = By.id("searchTravellerNonFilter");
		exportButton = By.xpath("//*[@id='exportLinkTravellers']/a/img");

		sendMsgLinkSubject = By
				.xpath("//*[@id='ctl00_MainContent_txtSubject']");
		sendMsgLinkTextBody = By
				.xpath("//*[@id='ctl00_MainContent_txtMessage']");
		sendMsgLinkButton = By
				.xpath("//*[@id='ctl00_MainContent_btnSendMessage']");

		closePanelBtn = By.xpath(".//*[@id='closeTravelerDiv']/a/img");

		clickonanyTraveller = By.xpath("(//span[@class='float-left'])[last()]");

		travelDetails = By.xpath("//span[text()='Traveller Details']");

		expandedTravellerName = By.xpath("//h1[@class='ellipsis-text']/span");

		// ====//

		vismoImage = By.xpath("//img[contains(@src,'green_man')]");
		homeCountrySearchResult = By
				.xpath("//div[@class='autocomplete-results']//b");
		replyTo = By.id("ctl00_MainContent_txtReplyTo");
		ticketingStatusHeader = By
				.xpath("//h3[text()='Refine by Ticketing Status']");

		dateRangeDropdown = By.xpath("//select[@id='rangeSelect']");

		buildingType = By.xpath("//select[@id='buildingType']");

		selectAllInTravelerListHeader = By
				.xpath("//div[@class='actionHeader clearfix']//label[contains(text(),'Select All')]");

		hotelIconIntravelerListExpand = By
				.xpath("(//div[@id='segment-list']//span[@class='hotelIcon float-left'])[last()]");

		hotelInfoIntravelerListExpand = By
				.xpath("(//div[@id='segment-list']//span[@class='hotelIcon float-left'])[last()]/following-sibling::span[@class='supplier-name padR8perc blackText']");
		hideTravelDetails = By
				.xpath("(//a[@id='hide-tvl-detail-btn'])[last()]");
		showTravelDetails = By
				.xpath("(//a[@id='hide-tvl-detail-btn' and contains(text(),'Show Travel Details')])[last()]");
		buildingType = By.xpath("//select[@id='buildingType']");

		nameWithFirstAndLastName = By
				.xpath("(//div[@id='scrollDiv']//h2/label[contains(text(),',')])[last()]");
		emailIdInExpandedTravellerList = By
				.xpath("(//div[@id='full-detail-left']//a[contains(text(),'.com')])[last()]");
		phoneNoInExpandedTravellerList = By
				.xpath("(//span[@id='phoneNumber'])[last()]");
		homeCountryInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Home Country: ')])[last()]");
		vipStatusInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'VIP Status:')])[last()]");
		expatInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//div[@class='expatIcon blackText'])[last()]");

		moreTravellerDetailsInExpandedTravellerList = By
				.xpath("(//a[@id='full-tl-btn-left'])[last()]");

		manualTripLinkInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//a[text()='Manual Trip'])[last()]");
		getAllKnownTravelInExpandedTravellerList = By
				.xpath("(//a[@id='all-tvl-btn' and contains(text(),'Get All Known Travel')])[last()]");

		additionalTripInfoPopup = By
				.xpath("//div[@id='desktopModal']//span[contains(text(),'Additional Trip Information')]");
		allKnownTravelsPopup = By
				.xpath("//div[@id='allKnownTravels']//span[@class='tripDetailHeader']");

		asiaPacificTravellerCount = By
				.xpath("//div[@id='tileContent']//h1[contains(text(),'Asia &')]/../../following-sibling::div/div/h2");
		travelerNameInExpandedView = By
				.xpath("(//div[@id='scrollDiv']//h2/label)[last()]");

		// asiaAndPacific =
		// By.xpath("//h1[text()='Asia & the Pacific']/../../following-sibling::div//h2");
		createdTravelerName = By
				.xpath("//span[contains(text(),'<replaceValue>')]");
		profileDetails = By
				.xpath("//div[@id='scrollDiv']/table/tbody/tr[1]//div[1]//h2");
		tripDetails = By
				.xpath("//div[@id='scrollDiv']/table/tbody/tr[1]//div[contains(@id,'segment-list')]");

		travellerNamesList = By.xpath("//div[@id='scrollDiv']/ul//h2//span");
		lasttravellerNamesList = By
				.xpath("(//div[@class='travLazyloadingLoading']/../ul/li)[last()]");
		buildingsEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkOfficeLocationsEnabled");
		siteAdminUpdate = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
		successUpdationMsg = By.id("lblMessage");

		nameInTravelDetails = By
				.xpath("//div[@id='travelerDetail']//span[contains(text(),'<replaceValue>')]");
		departureLableInTravelDetails = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Departure')])[last()]");
		arrivalLableInTravelDetails = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Arrival')])[last()]");

		mapUI = By.xpath("//a[@class='leaflet-control-zoom-in']");
		buildingFilterOption = By.xpath("//div[@id='buildingsTab']");
		addNewBuildingBtn = By.xpath("//button[@id='addBuilding']");

		enterNameinNewBuild = By.xpath("//input[@id='bname']");
		enterAddressinNewBuild = By.xpath("//input[@id='baddress']");
		enterCityinNewBuild = By.xpath("//input[@id='bcity']");
		enterCountryinNewBuild = By.xpath("//input[@id='bcountry']");
		saveBtnClickinNewBuild = By.xpath("//button[@id='btnSubmit']");
		searchResultsNewBuild = By.xpath("//input[@id='buildingsFind']");
		searchfromResults = By
				.xpath("(//h1[text()='otherbuilding111'])[last()]");
		searchforMapPoint = By.xpath(".//button[@id='btnFly']");

		countOfIntrenationalInLocationsPane = By
				.xpath("//div[@id='tileContent']//h1[contains(text(),'Asia &')]/../../div/../following-sibling::div[@class='clearfix']//span[contains(text(),'International')]/../span[2]");

		asiaAndPacific = By
				.xpath("//h1[text()='<replaceValue>']/../../following-sibling::div//h2");
		medicalExtremeCheckBox = By
				.xpath("//input[@id='extreme1']/following-sibling::label");
		medicalHighCheckBox = By
				.xpath("//input[@id='high1']/following-sibling::label");
		medicalMediumCheckBox = By
				.xpath("//input[@id='medium1']/following-sibling::label");
		medicalLowCheckBox = By
				.xpath("//input[@id='low1']/following-sibling::label");
		twoWaySMSEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk2waySMS");
		homeCountryVisibility = By
				.xpath("//div[@id='scrollDiv']/ul/li[1]/div/div/span");
		// =================??

		closeDetailPopUp = By.id("close-detail");
		clickProfileTips = By.id("ctl00_MiddleBarControl_linkProfile");
		arrivalCityErrorMsg = By
				.xpath("//div[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_arrivalCity_0_updatePanelSmartTextbox_0']//p[1]");
		evacuationNotificationschkbx = By
				.id("ctl00_MainContent_ProactiveEmails1_chkEvacuationNotifications");
		specialAdvisorieschkbx = By
				.id("ctl00_MainContent_ProactiveEmails1_chkSpecialAdvisories");
		travelUpdateschkbx = By
				.id("ctl00_MainContent_ProactiveEmails1_chkTravelUpdates");
		medicalAlerts = By
				.id("ctl00_MainContent_ProactiveEmails1_chkMedicalAlerts");
		travelAndBuildTrvelrRisks = By
				.id("ctl00_MainContent_ProactiveEmails1_chkSendEmailAffectedtraveller");
		proActBtnEmailUpdate = By
				.id("ctl00_MainContent_ProactiveEmails1_btnUpdate");
		proactiveEmailsNotification = By
				.id("ctl00_MainContent_ProactiveEmails1_lblNotification");
		manualEntrychkbx = By
				.id("ctl00_MainContent_TripSourceFilter1_chkManualEntry");
		myTripsEntrychkbx = By
				.id("ctl00_MainContent_TripSourceFilter1_chkMyTripsEntry");
		mobileCheckinEntry = By
				.id("ctl00_MainContent_TripSourceFilter1_chkMobileCheckinEntry");
		tripSourceFilter1_btnUpdate = By
				.id("ctl00_MainContent_TripSourceFilter1_btnUpdate");
		tripSourceFilter1_lblNotification = By
				.id("ctl00_MainContent_TripSourceFilter1_lblNotification");

		// embassyCheckbox = By.id("resourceTypeEmbassy");
		embassyCheckbox = By
				.xpath("//*[@id='resourceTypeEmbassy']//following-sibling::label");
		embassyIcon = By.className("clusterMarkerIcons resourceClusterIcon");
		embassyNames = By.xpath("//ul[@id='resources-list-container']/li");
		assistanceCenterCheckbox = By.id("resourceTypeAssCentre");
		clinicCheckbox = By.id("resourceTypeClinic");
		internationalSOSHeader = By
				.xpath("//div[@id='closeTileContent']/../preceding-sibling::span");
		riskLayerDropDown = By.xpath("//a[text()='Risk Layers']");
		TravelRisklayer = By.xpath("//label[text()='Travel']");
		medicalRisk = By.xpath("//li[@id='countryRiskType']/div/label");
		travelUpdate = By.xpath("//label[text()='Travel Update']");
		specialAdvisory = By.xpath("//label[text()='Special Advisory']");
		travelUpdateAlert = By.xpath("//label[text()='Travel Update']");
		medicalAlert = By.xpath("//label[@for='alertTypeMedical']");
		checkMedicalAlert = By.xpath("//input[@id='alertTypeMedical']");
		customerTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient");
		noTravellersFound = By.xpath("//div[text()='No travellers found']");
		vismoCustomer = By.xpath("//div[contains(@id,'vismocheckins')]//h1");

		additionalFiltersHeader = By
				.xpath("//h3[contains(text(),'Additional Filters')]");
		refineByTravellerHomeCountryHeader = By
				.xpath("//h3[contains(text(),'Home Country')]");
		refineByTravelDateHeader = By
				.xpath("//h3[contains(text(),'Refine by Travel Date')]");
		refineByRiskHeader = By
				.xpath("//h3[contains(text(),'Refine by Risk')]");
		refineByTravellerTypeHeader = By
				.xpath("//h3[contains(text(),'Refine by Traveller Type')]");
		extremeRatingLabel = By.xpath("//label[text()='Extreme']");
		highRatingLabel = By.xpath("//label[text()='High']");
		mediumRatingLabel = By.xpath("//label[text()='Medium']");
		lowRatingLabel = By.xpath("//label[text()='Low']");
		insignificantRatingLabel = By.xpath("//label[text()='Insignificant']");
		travelAlertImage = By
				.xpath("(//img[contains(@src,'country-icon')])[last()]");

		specialAdvisoryCheckbox = By.xpath("//label[@for='alertTypeSpecAd']");
		checkSpecialAdvisory = By.xpath("//input[@id='alertTypeSpecAd']");
		travelAlert = By.id("alertTypeTravel");
		travelAlertCheckbox = By.xpath("//label[@for='alertTypeTravel']");
		medicalAlertImage = By
				.xpath("(//img[contains(@src,'/plus-icon')])[last()]");

		specialAdvisoryAlertImage = By
				.xpath("(//img[contains(@src,'special_advisory')])[last()]");
		specialAdvisoryAlert = By.xpath("//label[text()='Special Advisory']");
		medicalRisklayer = By.xpath("//label[text()='Medical']");
		codeShareIcon = By.xpath("//span[contains(@title,'code share')]");
		codeShareTravellerCount = By
				.xpath("//label[@class='travellerNumFlight']");

		deleteBtn = By.id("btnDelete");
		noBuildingmsg = By
				.xpath("//div[contains(text(),'No buildings found')]");
		okBtn = By.id("okBtn");

		zoomIn = By.xpath("(//h1[@title='Zoom in'])[last()]");
		medicalRiskRating = By
				.xpath("(//div[@title='Medical Risk']//span)[last()]");
		travelRiskRating = By
				.xpath("(//div[@title='Travel Security Risk']//span)[last()]");
		indiaCluster = By
				.xpath("//div[@id='map']/div[1]/div[2]/div[3]/div[7]/div/span[2]");
		// indiaInMap = By.xpath("//div[@id='locationMarker_58']/a/div");
		indiaInMap = By
				.xpath("//span[contains(@class,'locationArea') and contains(text(),'India')]");
		alertPoint = By
				.xpath("//div[@id='map']/div[1]/div[2]/div[3]/div[1]//span[2]");

		officeBuildingType = By.id("buildingTypeOffice");
		accommodationBuildingType = By.id("buildingTypeAccomodation");
		otherBuildingType = By.id("buildingTypeOther");
		buildingAccommodationImage = By
				.xpath("(//img[contains(@src,'building_preferred_hotel')])[last()]");
		buildingOtherImage = By
				.xpath("(//img[contains(@src,'building_other')])[last()]");
		clinicImage = By
				.xpath("(//img[contains(@src,'resources_clinic')])[last()]");
		embassyImage = By
				.xpath("(//img[contains(@src,'resources_embassy')])[last()]");

		flightResult = By
				.xpath(".//*[@id='tileContent']/div[1]/div/div[4]/ul/li[1]/div/div/div[1]/label[1]");
		arrivalFlight = By
				.xpath("(//div[@id='segment-list']/div/p/span[3])[last()]");
		departureFlight = By
				.xpath("(//div[@id='segment-list']/div/p/span[4])[last()]");

		buildingSortBy = By
				.xpath(".//*[@id='tileContent']//div[1]/div/div/div[2]/a/img");
		buildingNameSortBy = By.xpath(".//*[@id='buidingsBuildingNameField']");
		buildingTypeSortBy = By.xpath(".//*[@id='buidingsBuildingTypeField']");
		buildingCitySortBy = By.xpath(".//*[@id='buidingsCityField']");
		buildingCountrySortBy = By.xpath(".//*[@id='buidingsCountryField']");
		buildingGeoCodeSortBy = By
				.xpath(".//*[@id='buidingsGeocodeStateField']");
		buildingStateSortBy = By.xpath(".//*[@id='buidingsStateField']");
		btnFlyTo = By.xpath("(.//*[@id='btnFly'])[last()]");

		standardExportOption = By
				.xpath(".//*[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']/table/tbody/tr[2]//span/label");
		fullItineraryExportOption = By
				.xpath(".//*[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']/table/tbody/tr[3]//span/label");
		standardExportChkBox = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_rdoExportFormatStandard']");
		fullItineraryExportChkBox = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_rdoExportFormatFullItinerary']");
		updateBtnExportOption = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnUpdate']");
		updateMsgExportOption = By
				.xpath("//span[@id='ctl00_MainContent_ExportOptions_lblNotification']");

		currentLocChkBox = By
				.xpath(".//label[contains(text(),'Currently in location')]");

		alertReadMore = By
				.xpath("(.//span[contains(text(),'Read more')])[last()]");
		readMorePopup = By.xpath(".//div[@id='alertDetails']");
		readMorePopupDtls = By.xpath(".//h1[contains(@class,'btmPadding')]");
		readMorePopupDtls1 = By
				.xpath(".//span[contains(@class,'lineHeight alert-details-title')]");

		alertCountryName = By
				.xpath("(.//h1[contains(@class,'alertCountryName')])[last()]");
		buildingNameInList = By
				.xpath("//ul[@id='buildings-list-container']/li[<replaceValue>]//h1");
		buildingNamesCount = By
				.xpath("//ul[@id='buildings-list-container']/li");

		peopleCount = By
				.xpath("//h1[text()='Africa']/../..//following-sibling::div//h2");
		homeCountryDisplayed = By
				.xpath("//div[@id='scrollDiv']/ul/li[<replaceValue>]/div/div/span");
		homeCountriesList = By.xpath("//div[@id='scrollDiv']/ul/li");

		flightDetailsClick = By
				.xpath("(//div[@class='row-1 clearfix'])[last()]");

		refineTraveller = By
				.xpath("//h3[contains(text(),'Refine by Traveller Type')]");
		refineRisk = By.xpath("//h3[contains(text(),'Refine by Risk')]");
		characterCount = By.id("charCnt");
		addResponse = By
				.xpath("(//input[@placeholder='click to add response'])[last()]");

		travelDetail = By.xpath("(//div[@class='fltLeft travellers'])[last()]");
		codeShareOption = By
				.xpath("(//span[@title='Ambiguous Travel - open Jaw'])[last()]");
		totalPeople = By.xpath("//div[@class='traveler-count']");
		airLineErrorMsg = By.xpath("//div[text()='Airline not found.']");
		travellerErrorMsg = By.xpath("//div[text()='No travellers found']");

		toCalenderIcon = By.xpath(".//*[@id='toCalendarIcon']");
		hydLocationMarker = By.cssSelector("#locationMarker_HYD a span");
		intermsearchresult = By
				.cssSelector(".search-scroll-area.serach-border-none li:nth-child(1)");

		selectsTravellerChkBox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		travellerListPane = By.xpath("//div[@id='travelerList']");

		trainChkBox = By.xpath("//label[@class='trave']");
		personArrowofTraveller = By
				.xpath("(//span[@class='people_arrow'])[last()]");
		lazyLoadingImage = By.xpath("//div[@id='scrollDiv']/div/div/img");

		sortByInBuildingsTab = By
				.xpath("//div[@id='closeTileContent']/following-sibling::div//a[@class='my_sortbuttonlink']");
		buildingNameSortByInBuildingsTab = By
				.xpath("//li[@id='buidingsBuildingNameField']");
		buildingTypeSortByInBuildingsTab = By
				.xpath("//li[@id='buidingsBuildingTypeField']");
		buildingCitySortByInBuildingsTab = By
				.xpath("//li[@id='buidingsCityField']");
		buildingCountrySortByInBuildingsTab = By
				.xpath("//li[@id='buidingsCountryField']");
		buildingGeoCodeSortByInBuildingsTab = By
				.xpath("//li[@id='buidingsGeocodeStateField']");
		buildingStateSortByInBuildingsTab = By
				.xpath("//li[@id='buidingsStateField']");
		btnFlyToInBuildingsTab = By.xpath("(//a[@id='btnFly'])[last()]");

		sortByInResourcesTab = By
				.xpath("//div[@id='closeTileContent']/following-sibling::div//a[@class='my_sortbuttonlink']");
		buildingNameSortByInResourcesTab = By
				.xpath("//li[@id='resourcesBuildingNameField']");
		buildingTypeSortByInResourcesTab = By
				.xpath("//li[@id='resourcesBuildingTypeField']");
		buildingCitySortByInResourcesTab = By
				.xpath("//li[@id='resourcesCityField']");
		buildingCountrySortByInResourcesTab = By
				.xpath("//li[@id='resourcesCountryField']");
		buildingGeoCodeSortByInResourcesTab = By
				.xpath("//li[@id='resourcesGeocodeStateField']");
		buildingStateSortByInResourcesTab = By
				.xpath("//li[@id='resourcesStateField']");
		btnFlyToInResourcesTab = By.xpath("(//a[@id='btnFly'])[last()]");

		uberTab = By.xpath("//div[@id='uberTab']");
		uberChkBoxinMappoints = By.xpath("//input[@id='chkUber']");
		uberPointsonMap = By
				.xpath("//img[contains(@class,'leaflet-marker-icon align-checkin-marker')]");
		showAllSegmentsDropDown = By.xpath("//select[@id='segmentFilter']");
		locationsTravellerCount = By
				.xpath("//div[contains(text(),'People') or contains(text(),'Person')]");
		TRAndTimerSymbol = By
				.xpath("(//a[contains(@class,'complianceTracker')]//span)[last()]");

		uberTraveller = By.xpath("(//div[@class='block_box']/h1)[last()]");
		uberOption = By.xpath("//span[@class='uber_newIcon float-left']");
		uberDuration = By
				.xpath("//span[@class='padR15']//following-sibling::span");
		uberDropOffs = By.xpath("//option[@id='uberOnly']");
		uberFirstTraveller = By
				.xpath("(//h2[@class='traveler_title']/span)[last()]");
		uberNarrowViewOption = By
				.xpath("//span[@class='right-margin fixed-name']");
		ubernarrowViewDuration = By
				.xpath("//span[@class='seg-head-location-days']");

		travellersListLinkInCheckInsTab = By.id("travelerlistLink");
		checkInsOnlyinShowAllSegmentDropDown = By
				.xpath("//option[@id='checkInsOnly']");
		selectTravellerfromCheckINsList = By
				.xpath("//div[@id='tileContent']//ul[@id='checkins-list-container']/li/div/div");
		blueManImageinCheckIn = By.xpath("//img[contains(@src,'blue_man')]");
		travellerSearchInTravellersPane = By
				.xpath("//*[@id='scrollDiv']//*[contains(text(),'<replaceValue>')]");

		uberMesgIcon = By.xpath(".//a[@id='messageLink1']/img");
		uberTravellerChkBoxDynamic = By
				.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//input[@class='trave']");
		uberMsgWindowCloseBtn = By
				.xpath("//div[contains(@class,'close-comm-panel')]//a//img");
		// div[@class='close-comm-panel close-popup']//a//img

		allMessagesHeaderinCommHistory = By
				.xpath("//*[contains(text(),'All messages')]");
		showFiltersBtninCommHistory = By
				.xpath("//*[contains(@id,'btnShowHideFilters')]");
		listofMessagesTableinCommHistory = By
				.xpath("//*[contains(@id,'gvResults')]");
		lastSubjectfromtheListofMessages = By
				.xpath("(//*[contains(@id,'lnkMessageID')])[last()]");
		frameinCommHistoryPage = By.id("ctl00_MainContent_ifrmCommDetails");
		filterRecipientsBtninCommHistory = By
				.xpath("//*[contains(@id,'btnFilterRecipients')]");
		resetFiltersBtninCommHistory = By
				.xpath("//*[contains(@id,'btnResetFilter')]");
		firstNameinRecipientsListofCommHistory = By
				.xpath("//*[contains(@id,'txtFirstName')]");
		lastNameinRecipientsListofCommHistory = By
				.xpath("//*[contains(@id,'txtLastName')]");
		phoneNumberinRecipientsListofCommHistory = By
				.xpath("//*[contains(@id,'txtPhoneNumber')]");
		emailinRecipientsListofCommHistory = By
				.xpath("//*[contains(@id,'txtEmailAddress')]");
		closePopUPFrameinCommHistory = By
				.xpath("//*[contains(@id,'imgCloseInvaliProfile')]");
		applyFiltersinShowFiltersCommHistory = By
				.xpath("//*[contains(@id,'btnSearch')]");
		msgCrntlyDisplayRecipientsinCommHistory = By
				.xpath("//*[contains(@id,'btnSendMessage')]");

		frameinRecipientslistSendMessage = By
				.id("ctl00_MainContent_ifrmSendMessage");
		sendanewmessageLabelinFrameinCommHistory = By
				.xpath("//*[contains(text(),'Send a new message')]");
		closePopUpFrameofSendMessageinCommHistory = By
				.xpath("//*[contains(@id,'imgCloseSendMessage')]");
		applyFiltersinRecipientsPageCommHistory = By
				.xpath("//*[contains(@id,'btnApplyFilter')]");
		searchResultChkbx = By
				.cssSelector(".search-container li:nth-of-type(1) .custome_checkbox label");
		searchResultTravCount = By
				.cssSelector(".search-container li:nth-of-type(1) .travellerNumFlight");
		searchResultTravCountForHotels = By
				.cssSelector(".search-container li:nth-of-type(1) .travellerNumHotel ");
		countryCode = By.xpath("//select[contains(@id,'drpCountryCode')]");
		searchResultTravellerCountinPane = By
				.xpath("//div[@id='travelerlistLink']/div/div[@class='traveler-count']");

		uberTimeCt = By.xpath("//p[@class='black-link']");
		uberTime = By
				.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//p[@class='black-link']");
		uberSortOption = By
				.xpath("//a[@class='my_sortbuttonlink']/img[@class='small']");
		uberSortDropOffTime = By.xpath(".//li[@id='uberageInSecondsField']");
		uberSortFirstName = By.xpath(".//*[@id='uberfirstNameField']");
		uberSortFirstNameAscArrow = By.xpath("//li[@id='uberfirstNameField']//img[contains(@src,'sort_asc')]");
		uberSortLastNameAscArrow = By.xpath("//li[@id='uberlastNameField']//img[contains(@src,'sort_asc')]");
		uberSortLastName = By.xpath(".//*[@id='uberlastNameField']");
		uberTravellersCt = By.xpath("//div[@class='block_box']//h1");
		uberTravellers = By
				.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//div[@class='block_box']//h1");

		labelName = By
				.xpath("//div[@id='full-detail-left']//span[contains(text(),'<replaceValue>')]");
		travellerFirstName = By
				.xpath("//tbody[@class='msg-list-container']//span[contains(text(),'<replaceValue>')]");

		loadingText = By.xpath("//a[text()='Loading...']");

		refreshVismo = By.id("refeshVismoList");
		vismoSearchBox = By.id("vismoCheckInsFind");

		showTravellersFrominUserSettings = By
				.xpath("//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/span[contains(text(),'Show Travellers From')]");
		availableGroupsinUserSettings = By
				.xpath("//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/table//td[contains(text(),'Available')]");
		selectedGroupsinUserSettings = By
				.xpath("//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/table//td[contains(text(),'Selected')]");
		updateBtninUserSettings = By
				.id("ctl00_MainContent_AssignGroupToUser1_btnSave");

		userSettings = By.xpath("//a[text()='User Settings']");
		userSettingsUpdate = By.xpath("(//input[@value='Update'])[last()]");

		availableGrpuserSettings = By
				.xpath("//select[contains(@id,'lstAvailableGroup')]");
		rightArwinUserSettings = By.xpath("//input[@id='ctl00_MainContent_AssignGroupToUser1_btnAdd']");

		travelerType = By.xpath("//div[@id='segment-list']/div/p/span");
		vismoTypeInExpandedView = By.xpath("//span[text()='Vismo check-in']");
		assistanceAppTypeInExpandedView = By
				.xpath("//span[text()='Assistance App check-in']");
		vismoCheckBox = By.xpath("//label[@for='chkVismoCheckins']");
		appUserLabelInExapndedView = By
				.xpath("//span[contains(text(),'App User')]");
		homeCountryDetails = By
				.xpath("//div[@id='travelerDetail']//div[@class='contact']/span[2]");
		refreshUber = By.id("refeshUberList");
		dropOffInfo = By
				.xpath("//div[@id='travelerDetail']//div[@class='contact']//span[4]");
		uberTravelerDateAndTime = By
				.xpath("//ul[@id='uber-list-container']/li[1]//p");
		uberTravelerDate = By
				.xpath("//div[@id='segment-list']//following-sibling::div[@class='cityCountry']/div[@class='fltRight width50 textRight']");
		uberDropOffDesc = By.xpath("//span[text()='Uber Drop-Off']");

		addLabelText = By.xpath("//textarea[@class='addLabelInput']");

		alertsChkBox = By.xpath("//input[@id='chkAlerts']");
		alertsImg = By
				.xpath("(//span[contains(@class,'alertsClusterIcon')])[last()]");

		checkinsImg = By
				.xpath("(//img[contains(@class,'leaflet-marker-icon align-checkin-marker checkins')])[last()]");

		uberChkBox = By.xpath("//input[@id='chkUber']");
		uberImg = By
				.xpath("//img[contains(@class,'leaflet-marker-icon align-checkin-marker uber')][last()]");

		vismoImg = By.xpath("(//img[contains(@src,'green_man')])[last()]");
		riskLayers = By.xpath("//a[text()='Risk Layers']");
		travelRiskRatingsOption = By
				.xpath("//label[@for='countryRiskTypeTravel']");
		medicalRiskRatingsOption = By
				.xpath("//label[@for='countryRiskTypeMedical']");
		showRiskRatingsCheckbox = By.xpath("//label[@for='chkCountryRisk']");
		onlyShowRiskRatingsCheckbox = By
				.xpath("//label[@for='chkTravellerFilter']");
		// appUserLabelInExapndedView =
		// By.xpath("//span[contains(text(),'App User')]");
		appUserLabelInNarrowView = By
				.xpath("//span[contains(text(),'App User')]");

		IntlResourcesImg = By
				.xpath("//img[contains(@class,'leaflet-marker-icon align-building-marker resource')]");
		last48HoursText = By.xpath("//span[text()='Last 48 Hours']");
		uberTravelerCount = By
				.xpath("//span[contains(@class,'travellerCount')]");
		uberSortBy = By.xpath("//img[@title='Sort By']");
		uberClosePanel = By.xpath("//div[@id='closeTileContent']/a/img");
		uberSearchbox = By.id("uberFind");
		uberImageOnMap = By.xpath("//img[contains(@src,'uber')]");
		uberDropOffTooltipText = By
				.xpath("(//span[contains(@class,'uber_newIcon')])[last()]");
		uberHeaderText = By.xpath("//span[contains(text(),'Uber Drop-Off')]");
		closeTravellerPanel = By.xpath(".//*[@id='closeTravelerDiv']/a/img");
		locationCountryZoom = By.xpath("//h1[text()='<replaceValue>']");
		peopleCtOnMap = By
				.xpath("//div[starts-with(@id,'locationMarker')]//span[text()='<replaceValue>']/..//div");
		uberOptionAfterExpand = By.xpath("//span[text()='Uber Drop-Off']");
		countryNameTooltip = By
				.xpath("//div[@id='tileContent']//ul/li[1]//h1/following-sibling::div//img");
		peopleTooltip = By.xpath("//div[@class='clearfix']");
		zoomInForAsiaAndPacific = By
				.xpath("//h1[contains(text(),'Asia & the Pacific')]/following-sibling::div//img");
		listOfCountries = By.xpath("//div[@id='tileContent']//ul/li");
		country = By.xpath("//div[@id='tileContent']//ul/li[<replaceValue>]");
		road = By.xpath("//button[text()='Road']");
		aerial = By.xpath("//button[text()='Aerial']");
		dark = By.xpath("//button[text()='Dark']");
		isosLogo = By.id("ctl00_LogoiSOS");
		controlRisksLogo = By.id("ctl00_LogoCR");
		zoomInForAmericas = By
				.xpath("//h1[text()='Americas']/following-sibling::div//img");
		zoomInLinkOnMap = By.xpath("//a[@title='Zoom in']");
		cityName = By.xpath("//h1");
		cityTravelerCount = By.xpath("//h2");
		// xMarkOnLabelTextbox = By.xpath("//a[text()='�']");
		xMarkOnLabelTextbox = By.xpath("//a[contains(@href,'#close')]");

		uberList = By.xpath("//div[contains(@class,'uber-scroll-area')]/ul/li");
		lastTravellerfromUberList = By
				.xpath("(//div[contains(@class,'uber-scroll-area')]/ul/li)[last()]");
		firstTravellerfromUberList = By
				.xpath("//div[contains(@class,'uber-scroll-area')]/ul/li[1]");
		travellerCountfromUber = By
				.xpath("//div[@id='travelerlistLink']/div/span[2]");
		uberTimefromList = By
				.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//p[@class='black-link']/span");

		searchResultsListPane = By.xpath("//div[@id='tileContent']//li");
		searchResultsfromList = By
				.xpath("//div[@id='tileContent']//li[<replaceValue>]/span");

		peopleCtForHotel = By
				.xpath("(//div[@id='tileContent']//label)[last()]");
		peopleCtArrowImg = By.cssSelector(".people_arrow>img");
		hotelAddress = By.xpath(".//*[@id='tileContent']//li//div//div[2]");
		nonFilterClick = By.xpath(".//*[@id='hotelsNonFilter']");

		firstSearchResultsfromList = By
				.xpath("//div[@id='tileContent']//li[1]/span");

		arrivingDeparting = By
				.xpath("//select//option[@value='arrivingDeparting']");

		checkInTraveller = By
				.xpath("(//ul[@id='checkins-list-container']//h1)[last()]");
		checkInMapPoint = By.xpath("//div[@class='leaflet-marker-pane']//img");

		alertSecurityImage = By
				.xpath("//img[contains(@src,'images/alert_security')]");
		locationPanelHeader = By
				.xpath("//div[@id='tileContent']//div[@class='panelHeader']");
		firstResultfromLocationPaneHeader = By
				.xpath("//div[@id='tileContent']//div[@class='locationsListing']//li[1]//h1");
		peoplePanelHeader = By
				.xpath("//div[@id='travelerList']//div[@id='travelerlistLink']//div[1]/div");
		firstResultfromPeoplePaneHeader = By
				.xpath("//div[@id='scrollDiv']//li[1]//h2/span");

		trainNameAndNum = By
				.xpath("//div[@id='tileContent']//ul/li[1]//section/following-sibling::label[1]");
		trainTravelerCount = By
				.xpath("//div[@id='tileContent']//ul/li[1]//section/following-sibling::label[2]");
		trainsCount = By.xpath("//div[@id='tileContent']//ul/li");
		trainNameInList = By
				.xpath("//div[@id='tileContent']//ul/li[<replaceValue>]//section/following-sibling::label[1]");
		trainDepartureDateAndTime = By
				.xpath("//div[@id='tileContent']//div[@class='search-container']//li[1]//div/div[2]/div[1]/div[2]");
		trainArrivalDateAndTime = By
				.xpath("//div[@id='tileContent']//li[1]//div/div[2]/div[2]/div[2]");
		noTrainFound = By.xpath("//div[text()='Train not found.']");

		toolTipbesideSendBy = By.className("send-cnt-label-tooltip");
		toolTipContent = By.className("ui-tooltip-content");
		toolTipContentLink = By.xpath("//div[@class='ui-tooltip-content']//a");

		clickOnState = By.xpath("(//div[@id='tileContent']//h1)[last()]");
		getStatePeopleCt = By.xpath("(//div[@id='tileContent']//h2)[last()]");
		countOnMap = By.xpath("//div[contains(@id,'locationMarker_')]//div");
		diffTravellerCt = By
				.xpath("//div[contains(@id,'location_')]//span[text()='<replaceValue>']//following-sibling::span");

		travellerChkBox = By.xpath("(//div[@id='scrollDiv']//input)[last()]");
		messageWindowCloseBn = By.xpath(".//*[@id='modal']//a/img");
		travellerEmaiID = By.xpath("(.//div[@id='scrollDiv']//div//a)[last()]");
		travellerPhoneNum = By
				.xpath("(.//*[@id='scrollDiv']//span[@class='phoneNum'])[last()]");
		moreTravellerLink = By.xpath("(.//a[@id='full-tl-btn-left'])[last()]");
		getAllKnownTraveller = By.xpath("(.//a[@id='all-tvl-btn'])[last()]");
		getAllKnownTravellerWindow = By
				.xpath("//div[@id='allKnownTravels']//div[@id='close-popup']");

		oneWayMessagelink = By
				.xpath("//div[@id='modal']//div[@class='comm-panel']//div[@class='comm-tabcontent']//ul/li[1]/a");
		forgotPasswordLink = By
				.id("ctl00_MainContent_LoginUser_lnkBtnForgotPassword");
		userNameinForgotPasswordPage = By
				.id("ctl00_MainContent_LoginUser_ucForgotPassword_txtUserId");
		submitBtninForgotPasswordPage = By
				.id("ctl00_MainContent_LoginUser_ucForgotPassword_btnUserIdSubmit0");
		securityAnswerField = By
				.id("ctl00_MainContent_LoginUser_ucForgotPassword_txtAnswer1");
		warningMessageInSecurityQuestionPage = By
				.xpath("//div[@id='ctl00_MainContent_LoginUser_ucForgotPassword_pnlSecurityQuestions']//legend//font");

		noHotelsFound = By.xpath("//div[text()='Hotel not found.']");

		locationLondon = By
				.xpath("//span[contains(text(),'[CITY] London, United Kingdom')]");
		LocOnMapCt = By
				.xpath("//span[text()='<replaceValue>']/preceding-sibling::div");
		locTravellerTypeCt = By
				.xpath("//h1[contains(text(),'LON')]/../../following-sibling::div//p/span[text()='<replaceValue>']//following-sibling::span");
		railLocTravellerTypeCt = By
				.xpath("//h1[contains(text(),'QQW')]/../../following-sibling::div//p/span[text()='<replaceValue>']//following-sibling::span");
		locationTilePplCt = By
				.xpath(".//*[contains(@id,'<replaceValue>')]//h2");
		locationTileName = By.xpath(".//*[contains(@id,'<replaceValue>')]//h1");

		viewOnMap = By.xpath("(//a[text()='View on map'])[last()]");
		viewOnMap1 = By
				.xpath("//div[text()='<replaceValue>']/following-sibling::div[1]/a[@id='btnFly']");
		expatImg = By
				.xpath("//div[@id='map']//div[@class='expatMarkerTag']//following-sibling::div//img");
		buildingsFlyIcon = By.xpath("(.//*[@id='btnFly'])[last()]");

		checkInTimeCt = By.xpath("//p[@class='black-link']");
		vismoTimeCt = By.xpath("//p[@class='black-link']");
		checkInTime = By
				.xpath("//ul[@id='checkins-list-container']//li[<replaceValue>]//p[@class='black-link']");
		vismoTime = By
				.xpath("//ul[@id='vismo-checkins-list-container']//li[<replaceValue>]//p[@class='black-link']");

		additionalFiltersinFiltersTab = By
				.xpath("//h3[@id='advanceFilterToggle']/span");
		refineByTripSourceHeader = By
				.xpath("//div[@id='filterScreen']/div[last()]//div[last()]/h3");
		travelAgentChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='travelAgent'][last()]");
		manualEntryChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='manualEntry'][last()]");
		myTrpsEntryChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='myTrips'][last()]");
		myTripsFrwdMailsChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='myTripsEmail'][last()]");

		assistanceAppCheckInChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='checkIn'][last()]");
		vismoCheckInChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='vismo'][last()]");
		uberDropOffChkbox = By
				.xpath("//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='uber'][last()]");
		// selectAllChkBoxfromLocationPane =
		// By.xpath("//div[@id='main']//div[@id='tileContent']//input[@id='showallLocation']");
		selectAllChkBoxfromLocationPane = By
				.xpath("//*[@id='showallLocationLabel']");
		exportBtninPeoplePane = By
				.xpath("//div[@id='travelerList']//div[@id='travelerlistLink']/div[last()]/div[@id='exportLink']/a");

		sendMsginPeoplePane = By.xpath("//div[@id='messageLink1']/a");
		travellersCtfromSendMsgScrn = By
				.xpath("//span[@class='nameTabFirstName']");

		advanceFilters = By.xpath("//h3[@id='advanceFilterToggle']");
		travelAgentCheckBox = By.xpath("//label[text()='Travel Agency']");
		manualEntryCheckBox = By.xpath("//label[text()='Manual Entry']");
		myTripsEntryCheckBox = By.xpath("//label[text()='MyTrips Entry']");
		myTripsEmailsCheckBox = By
				.xpath("//label[text()='MyTrips Forwarded Emails']");
		asstAppCheckBox = By.xpath("//label[text()='Assistance App check-in']");
		vismoCheckinCheckBox = By.xpath("//label[text()='Vismo check-in']");
		uberCheckBox = By.xpath("//label[text()='Uber Drop-Off']");
		myTripsFrwdMailsChkbox = By
				.xpath("//label[@for='myTrmyTripsEmailips']");
		myTripsFrwdMailsCheckbox = By
			    .xpath("//input[@id='myTripsEmail']");
		travellerspane = By
				.xpath(".//*[@id='tileContent']/div/div/div[contains(text(),'Locations')]");
		travellersCompletePanel = By
				.xpath(".//*[@id='tileContent']/div[1]/div/div");

		userSettingsDisplay = By
				.xpath("//span[text()='TravelTracker - User Settings']");
		manualEntry = By.xpath("//label[text()=' Manual Entry']");
		myTripsEntry = By.xpath("//label[text()=' My Trips Entry']");
		asstApp = By
				.xpath("//label[contains(text(),'  Assistance app check-in'])");

		selectAfricafrmTheList = By
				.xpath("//div[@id='tileContent']//h1[contains(text(),'Africa')]");

		// firsttravellerChkBox = By.xpath("firsttravellerChkBox");
		// verifySubjectInCommPage = By.xpath("//a[text()='<<replaceValue>>']");

		clickMoreTrvlDtls = By.xpath("//a[text()='More Traveller Details']");
		clickLessTrvlDtls = By.xpath("//a[text()='Less Traveller Details']");
		clickTrvlFromList = By
				.xpath("(//*[@id='scrollDiv']//span[contains(text(),'<replaceValue>')])[last()-1]");

		msgDtlsMsgdisplay = By
				.xpath("//input[@value='Message currently displayed recipients']");
		MsgRcptPopup = By.xpath("//h1[text()='Send a new message']");
		firsttravellerChkBox = By.xpath("//div[@id='scrollDiv']//li[1]//input");
		verifySubjectInCommPage = By
				.xpath("(//a[text()='<replaceValue>'])[last()]");// Need To
																	// Checkin
		verifySubjectInCommPageAndClk = By
				.xpath("(//a[text()='<replaceValue>'])[last()]");

		mapHomeCustDrpDwn = By
				.xpath("//ul[@id='nav']/following-sibling::div//select");
		ignoreTimeFilter = By.xpath("//*[@id='hotelsNonFilter']");

		vismoText = By
				.xpath("(//h1[contains(text(),'<replaceValue>')])[last()]");
		vismoHomeCountry = By.xpath("//span[contains(text(),'Home Country')]");

		hotelImg = By
				.xpath("//div[@id='map']//div[@class='hotelMarkerTag']//following-sibling::div//img");

		hotelCount = By.xpath("//div[@id='closeBtnSearch']/..");
		employeeId = By.xpath("//span[contains(text(),'Employee ID')]");
		moreTravellerDetailsInTravelDetails = By
				.xpath("(.//a[@id='full-tl-btn'])[last()]");

		narrowViewEmail = By
				.xpath("(//div[@class='display-inline']//a)[last()]");
		expandedViewEmail = By
				.xpath("//div[@class='contact']//a[@class='block_box']");
		expandedViewForSingleUserEmail = By
				.xpath("//div[@class='display-inline']//a[@class='block_box']");

		narrowViewPhoneNum = By.xpath("//span[@class='phoneNum'][last()]");
		expandedViewPhoneNum = By
				.xpath("//span[@class='block_box phoneNum'][last()]");

		choosePeopleExportColumns = By
				.cssSelector("#ctl00_MainContent_ExportOptions_Label2");
		availableHeaderinChoosePeople = By
				.cssSelector(".lable>td:nth-of-type(1)");
		selectedHeaderinChoosePeople = By
				.cssSelector(".lable>td:nth-of-type(3)");
		updateBtninChoosePeople = By
				.cssSelector("#ctl00_MainContent_ExportOptions_btnUpdate");

		userSettingsSelected = By
				.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']");
		userSettingsAvailable = By
				.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']");
		userSettingsUpdateBtn = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnUpdate']");
		userSettingsSuccessMsg = By
				.xpath("//span[text()='Excel Export Options updated successfully.']");
		userSettingsRightArrow = By.xpath("//input[@value='>>']");
		userSettingsLeftArrow = By.xpath("//input[@value='<<']");

		userSettingsUpArrow = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnMoveUp']");
		userSettingsDownArrow = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnMoveDown']");
		morethan1ColumnSelectErrMsg = By
				.xpath("//span[@id='ctl00_MainContent_ExportOptions_lblNotification']");

		// Need To Checkin
		chooseExpCol = By.xpath("//span[text()='Choose Export Columns']");
		availableListBox = By.xpath("//td[contains(text(),'Available')]");
		selectedListBox = By.xpath("//td[contains(text(),'Selected')]");

		travelUpdateCheckbox = By.xpath("//input[@id='alertTypeTravel']");
		MedicalAlertCheckbox = By.xpath("//input[@id='alertTypeMedical']");
		Noalertsfound = By
				.xpath("//div[contains(.,'No alerts found') and @class='panelCountErrorPrompt']");

		selectedColumnsInUserSettings = By
				.xpath("//select[contains(@id,'lstSelectedColumns')]//option");
		dynamicLocation = By
				.xpath("//h1[text()='<replaceValue>']//ancestor::div[3]//div[@class='clearfix']");
		MedicalAlertImageOnMap = By
				.xpath("//img[contains(@src,'alert_medical')]");
		TravelUpdateImageOnMap = By
				.xpath("//img[contains(@src,'images/alert_security')]");
		specialAdvisoryAlertImageOnMap = By
				.xpath("(//img[contains(@src,'special_advisory_new')])[last()]");
		ZoomInOnMap = By.xpath("//a[contains(@title,'Zoom in')]");
		travellerCheckboxInMapHomePage = By
				.xpath("//span[contains(text(),'<replaceValue>')]/../preceding-sibling::div");
		MedicalAlertOnMap = By
				.xpath("//img[contains(@src,'alert_medical')][last()]");

		intlSOSResourcesCountry = By
				.xpath("//label[contains(@for,'bcountry')]");

		refineByDataSource = By
				.xpath("//div[@id='filterScreen']/div[last()]//div[last()]/h3");

		emailFirstName = By
				.xpath("//input[(@class='nameTabFirstNameInput focus') and @type='text']");
		emailLastName = By
				.xpath("//input[(@class='nameTabFirstNameInput focus') and @type='text']");
		editoptiononsendmsgpage = By
				.xpath("//tbody[@class='msg-list-container']//tr[1]//span[@class='editdata']");
		alertTab = By.xpath("//div[@id='alertsTab']");
		searchwithinresult = By.xpath("//input[@id='alertsFind']");
		crossborderdetail = By
				.xpath("(//*[@class='alertCountryName' and contains(text(),'<replaceValue>')])[last()]");

		sendNewMessageTitle = By.cssSelector(".comm-panel-title");
		travellerHeader = By.cssSelector(".tableHeaderLeft");
		travellerNameTab = By.cssSelector(".nameTab");
		travellerAddRecipient = By.cssSelector("#addRecipientBtn");
		travellerContactArrowExpanded = By
				.xpath("//span[@id='travellerContactArrow' and contains(@class, 'arrow-down_filter collapseArrow')]");
		travellerContactArrowCollapsed = By
				.xpath("//span[@id='travellerContactArrow' and contains(@class, 'collapseArrow arrow-right_filter')]");
		localContactArrowCollapsed = By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'collapseArrow arrow-down_filter')]");
		localContactCollapsedarrow =By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'arrow-right_filter collapseArrow')]");
	
		localContactArrowExpanded = By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'collapseArrow arrow-right_filter')]");
		includeLocalContacts = By.cssSelector("#localContactsEB");

		travelReadyText = By
				.xpath("//span[@class='compliance-tracker' and contains(text(),'TR')]");
		travelReadySandSymbol = By
				.xpath("//span[@class='compliance-tracker']/img[@class='compliance-tracker-icon']");

		alertsTravellerCt = By
				.xpath("(//h1[contains(text(),'India')])[last()]/../following-sibling::div//h2");

		removeLocalContacts = By
				.xpath("//a[contains(text(),'Remove Local Contacts')]");

		alertsTravellerCtForIndia = By
				.xpath("(//h1[contains(text(),'India (Country)')])[last()]/../following-sibling::div//h2");
		deleteTravellerfromMsgWindow = By
				.xpath("//span[@id='deleteMsgTraveller']");

		statusNonVIP = By.xpath("//span[text()=' VIP Status: No  ']");
		statusVIP = By.xpath("//span[text()=' VIP Status:Yes  ']");

		flightDepartureAllKnownTravel = By
				.xpath("//div[@class='flightDetail']/p/following-sibling::span[contains(.,'<replaceValue>')]");
		flightNameAllKnownTravel = By
				.xpath("//div[@class='flightDetail']/p/following-sibling::"
						+ "span[contains(.,'<replaceValue>')]/../../div[@id='segment-list']/div[@class='flightList']/p/span[@class='right-margin supplier-name']");
		flightDepartureCity = By
				.xpath("//div[@class='flightDetail']/p/following-sibling::span[contains(.,'<replaceValue>')]/preceding-sibling::p/span");

		hotelCheckinAllKnownTravel = By
				.xpath("//div[@id='segment-list']/div/p/span[@class='trip-Arrivaltxt padR5perc']/span[@class='block_box' and contains(text(),'<replaceValue>')]");
		hotelNameAllKnownTravel = By
				.xpath("//div[@id='segment-list']/div/p/span[@class='trip-Arrivaltxt padR5perc']/span[@class='block_box' and contains(text(),'<replaceValue>')]/../preceding-sibling::span[@class='supplier-name padR8perc blackText']");

		oneWaySMS = By.xpath("//a[text()='One-way message']");
		appNotificationChkBox = By.xpath("//input[@id='chknotification']");
		twoWaySMSOption = By.cssSelector(".comm-nav-link.two-way");

		CheckinText = By
				.xpath("(//h1[contains(text(),'<replaceValue>')])[last()]");
		peopleCtForFlight = By
				.xpath("//div[@id='tileContent']//label[@class='travellerNumFlight']");
		filterCity = By.xpath("(//span[contains(text(),'[IATA:')])[last()]");
		searchfilter = By.xpath("//a[@id='searchTravellerNonFilterExpand']");
		travelerstripsegments = By
				.xpath("//a[@id='searchTravellerNonFilterExpand']");

		uberTravelerEndDate = By
				.xpath("//div[@id='segmentContainer']//div[@class='flightDetail']/span[2]");
		uberEnddateDetailrow = By
				.xpath("//div[@id='segment-list']//div[2]/div[@class='fltRight width50 textRight']");

		uberDropoffDays = By
				.xpath("By.xpath(//div[@class='clearfix block_box']/div//span[@class='myDateArriveDepart']/span[contains(text(),'day')])[last()]");
		uberTravellerEnddateinExpanded = By
				.xpath("//div[@class='clearfix block_box']/div//span[@class='myDateArriveDepart']/span[contains(text(),'|')])[last()]");
		uberEnddateDetailRowInExpanded = By
				.xpath("//div[@id='segment-list']//span[contains(@class,'Departure')])[last()]");

		listOfCities = By.xpath("//div[@id='tileContent']//ul/li");

		cities = By.xpath("//div[@id='tileContent']//ul/li[<replaceValue>]");
		radiobutton = By
				.xpath("//div[@class='custome_radiobox']/label[text()='km']");
		alerts = By
				.xpath("//img[contains(@class,'leaflet-marker-icon')][last()]");

		searchTraveller = By
				.xpath("//h2//span[contains(text(),'<replaceValue>')]");
		disableincludelocalcontact = By
				.xpath("//a[contains(@class,'addRecipientBtn disableButton')]");
		noTravellerFoundError = By
				.xpath("//div[contains(text(),'No travellers found')]");
		localcontacttraveller = By.xpath("//div[@class='countryLogo'][last()]");
		trainSectionLabel = By.xpath("//div[contains(text(),'Train')]");

		unreachableTravellers = By
				.xpath("//div[@id='scrollDiv']//label[contains(text(),'<replaceValue>')]");
		emailID = By
				.xpath("//table[@id='gvRecipients']//span[contains(text(),'<replaceValue>')]/../following-sibling::td[3]");

		localcontactcollapse = By
				.xpath("//span[@id='EBContactArrow' and contains(@class, 'arrow-right_filter collapseArrow')]");
		applyDefaultOrder = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnSetSelectedToDefaultOrder']");

		exportColumnsMoveUpBtn = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnMoveUp']");
		exportColumnsMoveDownBtn = By
				.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnMoveDown']");
		multiOptionMoveError = By
				.xpath("//span[contains(text(),'choose only one field at a time')]");
		uberTravellerInUber = By
				.xpath("//ul[@id='uber-list-container']//li//h1[contains(text(),'<replaceValue>')]");

		vismoCheckinDateConv = By.xpath("//span[@class='pipeLine']");
		TrustelogoImage=By.xpath("//a/img[@alt='TRUSTe Privacy Certification']");
		homeCountryInMap = By.xpath("//div[@class='contact']/span[contains(text(),'Home Country')]");
	}

	}
