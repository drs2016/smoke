package smoke.objects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class TTMobilePage  {

	public static By userName = By.id("username");;
	public static By password = By.id("password");
	public static By signInBtn = By.xpath("//span[text()='Sign In']");
	public static By logoutBtn = By.xpath("//span[@class='logout']");
	public static By filterBtn = By.id("filter-btn");
	public static By presetRadioBtn = By.id("presetRadio");
	public static By nowCheckbox = By.id("Now");
	public static By last31DaysCheckbox = By.id("last31");
	public static By next24HoursCheckbox = By.id("Next24h");
	public static By next1To7DaysCheckbox = By.id("Next1to7d");
	public static By next8To31DaysCheckbox = By.id("Next31d");
	public static By arriving = By.id("arriving");
	public static By departing = By.id("departing");
	public static By inLocation = By.id("inLocation");
   	//public static By okBtn = By.id("ok-btn");
	public static By okBtn = By.cssSelector("#ok-btn .icon-text");
	public static By loadingImage = By
			.xpath("//span[@class='filter-load-text']");
	public static By totalTravellerCounts = By
			.xpath("//div[@id='content']//li//button");
	public static By travellerCount = By
			.xpath("//div[@id='content']//li[<replaceValue>]//button/span");
	//public static String firstName = "Project_auto" + generateRandomNumber();
	
	public static By loginLoadingImage = By
			.xpath("//span[@class='filter-load']");
	public static By cancelButtonInAddRecipients = By
			.xpath("//a[text()='Cancel']");
	public static By firstNameErrorMsg = By.id("addRecipientInputErrFname");
	public static By lastNameErrorMsg = By.id("addRecipientInputErrLname");
	public static By emailErrorMsg = By.id("addRecipientInputErrEmail");
	public static By mobileErrorMsg = By.id("addRecipientInputErrMobile");

	public static By sendMessage = By.xpath(".//div[@id='messageLink1']");
	public static By editOption = By
			.xpath("(//span[@class='editdata'])[last()]");
	public static By firstNameEditMode = By
			.xpath("//input[@class='nameTabFirstNameInput focus']");
	public static By lastNameEditMode = By
			.xpath("//input[@class='nameTabLastNameInput focus']");
	public static By emailEditMode = By
			.xpath("//input[@class='emailTabEmailInput focus']");
	public static By phoneEditMode = By
			.xpath("//input[@class='phoneTabPhoneInput focus']");
	public static By saveBtn = By.xpath("(//span[@class='saveEdit'])[last()]");
	public static By firstNameErrorMessage = By
			.xpath("(//span[@class='eroorText errorFirstName'])[last()]");
	public static By emailerrorMsg = By
			.xpath("(//span[@class='eroorText errorEmailAddress'])[last()]");

	public static By enterEmail = By.xpath(".//*[@id='advanceCopyTo']");
	public static By enterSubject = By.xpath(".//*[@id='msgsubject']");
	public static By enterMsgBody = By.xpath(".//*[@id='msgbody']");
	public static By sendBtn = By.xpath("//a[text()='Send']");
	public static By sentMsg = By
			.xpath("//div[@class='msg-send-notification']");

	// Oct 20
	public static By countryName = By
			.xpath("//td[@class='phoneTab']//span[contains(text(),'India ')]");
	public static By copyAndReportTo = By.id("advanceCopyTo");
	public static By copyAndReportToErrMsg = By.id("sendMsgError");
	public static By sendMsgSuccessMsg = By
			.xpath("//div[text()='Your message is being sent. View Communication History for responses.']");
	public static By alerts = By.xpath("//span[text()='Alerts']");

	// Oct 21
	public static By travelExtremeCheckbox = By.id("extreme");
	public static By travelHighCheckbox = By.id("high");
	public static By travelMediumCheckbox = By.id("medium");
	public static By travelLowCheckbox = By.id("low");
	public static By travelInsignificantCheckbox = By.id("insignificant");
	public static By medicalExtremeCheckbox = By.id("extreme1");
	public static By medicalHighCheckbox = By.id("high1");
	public static By medicalMediumCheckbox = By.id("medium1");
	public static By medicalLowCheckbox = By.id("low1");
	public static By homeCountry = By.id("country");
	public static By travelUpdateCheckbox = By.id("travel1");
	public static By medicalCheckbox = By.id("medical1");
	public static By specialAdvisoriesCheckbox = By.id("specialAdvisories1");
	public static By travellersPresentCheckbox = By.id("travellersPresent");

	public static By rangeRadioBtn = By
			.xpath("//div/input[@id='rangeRadio']/following-sibling::label");
	//public static By rangeArrivingChkBox = By.xpath("//input[@id='arriving']/following-sibling::label");
	public static By rangeArrivingChkBox =By.id("arriving");
//public static By rangeDepartingChkBox = By.xpath("//input[@id='departing']/following-sibling::label");
	public static By rangeDepartingChkBox=By.id("departing");
//public static By rangeInlocationChkBox = By.xpath("//input[@id='inLocation']/following-sibling::label");
	public static By rangeInlocationChkBox =By.id("inLocation");
	
	public static By fromCalendarIcon = By.cssSelector("#fromCalendarIcon");
	public static By alertsTravellerCt = By
			.xpath("//div[@id='content']//li//span/following-sibling::span");
	public static By alertstravellerCount = By
			.xpath("//div[@id='content']//li[<replaceValue>]//span/following-sibling::span");

	public static By inputSearchBox = By.xpath(".//input[@id='username']");
	public static By searchButton = By
			.xpath("//span[contains(text(),'Search')]");
	public static By placesNamed = By.cssSelector("#searchPlaces>span");
	public static By flightsCalled = By.cssSelector("#searchFlights>span");
	public static By travelersNamed = By.cssSelector("#searchTravelers>span");
	public static By placesArrowSearch = By
			.cssSelector("#searchPlaces .arrow-search.fltRight");
	public static By countrySearch = By
			.cssSelector(".search-list li:nth-child(1) span:nth-child(2)");
	public static By placeResults = By.cssSelector(".cityCount");
	public static By flightArrowSearch = By
			.cssSelector("#searchFlights .arrow-search.fltRight");
	public static By flightResults = By.cssSelector(".cityCount");
	public static By travelersArrowSearch = By
			.cssSelector("#searchTravelers .arrow-search.fltRight");
	public static By travelerResults = By.cssSelector(".cityCount");
	
	//Need To Checkin
	public static By locationTab = By.xpath("//div[@id='actions']//ul//li[@class='tab locationsTab activeTab']");
	public static By clickanyTravellerinLoc = By.xpath("(//div[@id='content']//li//button)[last()]");
	public static By clickRightArrow = By.xpath("(//div[@id='content']//ul//li//a//div[@class='rightArrow'])[last()]");
	public static By clickExportLink = By.id("exportLink");
	public static By clickDownloadLink = By.id("exportDwnld");
	
	public static By clickanyTravellerinAlerts = By.xpath("(//div[@id='content']//li//span/following-sibling::span)[last()]");
	
	public static By sortByCountryInTTMobile = By.id("sortByCountry");
	public static By sortByTravellerInTTMobile = By.id("sortByCount");
	public static By alertCountriesCount = By.xpath("//div[@class='locationsListing']/ul/li");
	//public static By countryNameInAlertsList = By.xpath("(//a[@id='alertId_<replaceValue>'])[Last()]/h1");
	public static By countryNameInAlertsList = By.xpath("//a[@id='alertId_<replaceValue>']/h1");
	public static By countryCount = By.xpath("//div[@id='content']//li[<replaceValue>]//div/span[not(contains(@class,'icon'))]");
	public static By messageHeader = By.id("messageHeader");
	public static By messageBody = By.xpath("//div[@id='bodyDiv']/textarea");
	public static By travellerPhoneNo = By.xpath("(//div[@id='number-row']/section/div/label)[last()]");
	public static By travellerEmail = By.xpath("//div[@id='email-row']/section//label");
	public static By sendMessageSuccessConfirmation = By.xpath("//span[text()='Your message has been sent...']");
	public static By enableTwoWaySMSResponseCheckbox = By.id("travel2way");
	public static By phoneNo = By.xpath("//div[@id='number-row']/input");
	public static By firstTravelerCheckBox = By.xpath("//ul/li[1]//input/following-sibling::label");
	public static By travellerCountUnderLocations = By.xpath("//ul/li[1]/a/button");
	public static By fetchingDetailsLoadingImage = By.xpath("//div[text()='Fetching Details..']");
	public static By travelerPhoneNo = By.xpath("//a[@class='phone']/p");
	public static By goBtn = By.xpath("//*[@id='goBack']/a/span");
	public static By location = By.xpath("//span[text()='<replaceValue>']");
	public static By internationalCheckbox = By.xpath("//input[@id='international1']/following-sibling::label[@for='international1']");
    public static By domesticCheckbox = By.xpath("//input[@id='domestic1']/following-sibling::label[@for='domestic1']");
	public static By travellerHomeCountriesCount = By.xpath("//div[@id='content']//ul/li");
	public static By travellerHomeCountryName = By.xpath("//div[@id='content']//ul/li[<replaceValue>]//div/span");
	
	public static By lastTravellerChkBox = By.xpath("(//div[@class='customCheckBox'])[last()]");
	public static By messageButton = By.xpath("//div[text()='Message']");
	public static By addTraveller = By.xpath("//a[@id='addTraveller']");
	public static By addTravellerLastName = By.xpath("//input[@id='lastName']");
	public static By addTravellerFirstName = By.xpath("//input[@id='firstName']");
	public static By addTravellerPhoneNum = By.xpath("//input[@id='phoneNumber']");
	public static By addTravellerEmailAdd = By.xpath("//input[@id='emailAddress']");
	
	public static By addTravellerEditPhoneNum = By.xpath("(//input[@value='<replaceValue>'])[last()]");
	public static By addTravellerEditEmail = By.xpath("(//input[@value='<replaceValue>'])[last()]");
	
	public static By phoneNumberInMsgWindow = By.xpath("(//div[@id='number-row']//section//following-sibling::input)[last()]");
	public static By emailInMsgWindow = By.xpath("(//div[@id='email-row']//section//following-sibling::input)[last()]");
	public static By medicalAlerticon = By.xpath("(//span[contains(@class,'iconMedical')])[last()]");
	public static By refinebytraveltypeOption=By.xpath("//label[contains(text(),'<replaceValue>')]");
	public static By expatriatetraveller=By.xpath("//span[contains(text(),'<replaceValue>')]/following-sibling::ol/li/table/tbody/tr/td[contains(text(),'Expatriate')]");
	
	public static By addRecipient = By.xpath("//a[@id='addTraveller']");
	public static By firstnameInTTMobile = By.xpath("//input[@id='firstName']");
	public static By lastnameInTTMobile = By.xpath("//input[@id='lastName']");
	public static By countrycodeInTTMobile = By.xpath("//input[@id='countryCode']");
	public static By phonenumberInTTMobile = By.xpath("//input[@id='phoneNumber']");
	public static By emailAddressInTTMobile = By.xpath("//input[@id='emailAddress']");
/*	public boolean datePresetsSelection(String travelType, String checkOption)
			throws Throwable {
		boolean result = true;
		LOG.info("datePresetsSelection method execution started");
		List<Boolean> flags = new ArrayList<>();

		if (isElementNotSelected(presetRadioBtn)) {
			flags.add(JSClick(presetRadioBtn,
					"Preset Radio Button under Filters Button"));
		}
		if (travelType.equalsIgnoreCase("Arriving")) {
			if (isElementNotSelected(arriving))
				flags.add(JSClick(arriving,
						"Arriving Checkbox under Filters Button"));
			if (isElementSelected(departing))
				flags.add(JSClick(departing,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(inLocation))
				flags.add(JSClick(inLocation,
						"In Location Checkbox under Filters Button"));
		} else if (travelType.equalsIgnoreCase("InLocation")) {
			if (isElementNotSelected(inLocation))
				flags.add(JSClick(inLocation,
						"In Location Checkbox under Filters Button"));
			if (isElementSelected(departing))
				flags.add(JSClick(departing,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(arriving))
				flags.add(JSClick(arriving,
						"Arriving Checkbox under Filters Button"));
		} else if (travelType.equalsIgnoreCase("Departing")) {
			if (isElementNotSelected(departing))
				flags.add(JSClick(departing,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(inLocation))
				flags.add(JSClick(inLocation,
						"In Location Checkbox under Filters Button"));
			if (isElementSelected(arriving))
				flags.add(JSClick(arriving,
						"Arriving Checkbox under Filters Button"));
		}

		switch (checkOption) {
		case "Now":
			if (isElementNotSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox,
						"Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox,
						"Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox,
						"Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox,
						"Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "last31DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementNotSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox,
						"Clicked to check 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox,
						"Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox,
						"Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox,
						"Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next24HoursCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox,
						"Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementNotSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox,
						"Clicked to check 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox,
						"Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox,
						"Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next1To7DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox,
						"Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox,
						"Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementNotSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox,
						"Clicked to check 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox,
						"Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next8To31DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox,
						"Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox,
						"Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox,
						"Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementNotSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox,
						"Clicked to check 'next8To31DaysCheckbox'"));
			break;

		}
		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		waitForInVisibilityOfElement(loadingImage,
				"Loading Image in the Map Home Page");
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("datePresetsSelection method execution completed");
		return result;
	}

	public boolean clickFiltersBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFiltersBtn function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickOnFiltersBtn function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickOnFiltersBtn function execution failed");
		}
		return flag;
	}

	*//**
	 * @author: Arun
	 *  Implemented On : 20-10-2016
	 *  : This Component will fetch the Travelers count from the Application
	 *        The test case is 'Mobile_Locations_Presets_Search_Arriving'
	 *//*
	public int getTravellersCount() throws Throwable {
		boolean flag = true;
		LOG.info("getTravellersCount function execution Started");

		List<Boolean> flags = new ArrayList<>();
		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Travelers
		int count = 0;
		String text;
		List<WebElement> list = Driver
				.findElements(TTMobilePage.totalTravellerCounts);
		for (WebElement c : list) {
			count++;
		}
		System.out.println("count : " + count);
		try {
			for (int i = 1; i <= count; i++) {

				countryIndex = Integer.toString(i);
				List<WebElement> travellers = Driver
						.findElements(createDynamicEle(
								TTMobilePage.travellerCount, countryIndex));
				for (WebElement e : travellers) {
					text = e.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
					System.out.println(total_count);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total_count;
	}

	public boolean selectRefineByRisk(String travelRisk, String medicalRisk)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByRisk function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(clickFiltersBtn());
			switch (travelRisk) {
			case "travel_extreme":
				if (isElementNotSelected(travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_high":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_medium":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_low":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_insignificant":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;
			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage,
					"Loading Image in the Map Home Page");

			switch (medicalRisk) {
			case "medical_extreme":
				if (isElementNotSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_high":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_medium":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_low":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;
			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByRisk function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectRefineByRisk function execution failed");
		}
		return flag;
	}

	public boolean selectRefineByAlertType(String alertType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByAlertType function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(clickFiltersBtn());
			switch (alertType) {
			case "Travel Update":
				if (isElementNotSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(
							TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(
							TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(
							TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Medical":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(
							TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(
							TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(
							TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Special Advisories":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(
							TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(
							TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(
							TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Travellers Present":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(
							TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(
							TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(
							TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByAlertType function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectRefineByAlertType function execution failed");
		}
		return flag;
	}

	public boolean rangeFilterSelection(String travelType) throws Throwable {
		boolean result = true;
		LOG.info("rangeFilterSelection method execution started");
		List<Boolean> flags = new ArrayList<>();
        
		if (travelType.equalsIgnoreCase("Arriving")) {
			if (isElementNotSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox,
						"Arriving Checkbox under Filters Button"));
			if (isElementSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox,
						"In Location Checkbox under Filters Button"));
			
		} else if (travelType.equalsIgnoreCase("InLocation")) {
			if (isElementNotSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox,
						"In Location Checkbox under Filters Button"));
			if (isElementSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox,
						"Arriving Checkbox under Filters Button"));			
		} else if (travelType.equalsIgnoreCase("Departing")) {
			if (isElementNotSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox,
						"Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox,
						"In Location Checkbox under Filters Button"));
			if (isElementSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox,
						"Arriving Checkbox under Filters Button"));			
		}

		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		waitForInVisibilityOfElement(loadingImage,
				"Loading Image in the Map Home Page");
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("rangeFilterSelection method execution completed");
		return result;
	}

	@SuppressWarnings("unchecked")
	public boolean setFromDateInDateRangeTab(int indexOfYear, int indexOfMonth,
			String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isNotVisible(fromCalendarIcon,
					"Check for 'From Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange,
						"Date Range Tab inside Filters tab");
			}
			assertElementPresent(TravelTrackerHomePage.fromCalendarIcon,
					"From Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.fromCalendarIcon,
					"From Calendar Icon inside the Date Range");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear,
					"Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth,
					"Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange,
					DayOfFromDate), "Day inside the Date Range");
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			switchToDefaultFrame();

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}

	*//**
	 * @author: Vivek
	 *  Implemented On : 24-10-2016
	 *  : This Component will fetch the Alerts Travelers count from the
	 *        Application
	 * 
	 *//*
	public int getAlertsTravellersCount() throws Throwable {
		boolean flag = true;
		LOG.info("getAlertsTravellersCount function execution Started");

		List<Boolean> flags = new ArrayList<>();
		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Travelers
		int count = 0;
		String text;
		List<WebElement> list = Driver
				.findElements(TTMobilePage.alertsTravellerCt);
		for (WebElement c : list) {
			count++;
		}
		System.out.println("count : " + count);
		try {
			for (int i = 1; i <= count; i++) {

				countryIndex = Integer.toString(i);
				List<WebElement> travellers = Driver
						.findElements(createDynamicEle(
								TTMobilePage.alertstravellerCount, countryIndex));
				for (WebElement e : travellers) {
					text = e.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
					System.out.println(total_count);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total_count;
	}

	public boolean selectSearchType(String searchType, String testdata1)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(searchButton, "Sarch Button"));			
			flags.add(type(inputSearchBox, testdata1, "input search box for the type"));			
			flags.add(waitForElementPresent(placesNamed, "places named", 30));
			flags.add(assertElementPresent(placesNamed, "places named"));
			flags.add(assertElementPresent(flightsCalled, "flights called"));
			flags.add(assertElementPresent(travelersNamed, "travelers named"));

			switch (searchType) {
			case "Places":
				flags.add(JSClick(placesArrowSearch, "place search Arrow"));
				flags.add(waitForElementPresent(countrySearch,
						"search country", 30));
				flags.add(JSClick(countrySearch, "Search country"));
				flags.add(waitForElementPresent(placeResults, "place results",
						30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults,
						"place results")) > 0, " Place results are not shown")));
				break;

			case "Flights":
				flags.add(JSClick(flightArrowSearch, "Flight search Arrow"));
				flags.add(waitForElementPresent(flightResults,
						"flight Results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(flightResults,
						"flight results")) > 0, " Flight results are not shown")));
				break;

			case "Travellers":
				flags.add(JSClick(travelersArrowSearch, "Flight search Arrow"));
				flags.add(waitForElementPresent(travelerResults,
						"traveler Results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(travelerResults,
						"Traveler results")) > 0,
						" Traveler results are not shown")));
				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("select Search Type function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("select Search Type function execution failed");
		}
		return flag;
	}
	
	public boolean selectRandomSearchType(String searchType, String searchValue)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(searchButton, "Sarch Button"));			
			//flags.add(type(inputSearchBox, testdata1, "input search box for the type"));	
			String travelerName = null;
			if (searchValue.equals(""))
				travelerName = ManualTripEntryPage.firstNameRandom;
			
			flags.add(type(TTMobilePage.inputSearchBox, travelerName, "Traveller search"));
			flags.add(waitForElementPresent(placesNamed, "places named", 30));
			flags.add(assertElementPresent(placesNamed, "places named"));
			flags.add(assertElementPresent(flightsCalled, "flights called"));
			flags.add(assertElementPresent(travelersNamed, "travelers named"));

			switch (searchType) {
			case "Places":
				flags.add(JSClick(placesArrowSearch, "place search Arrow"));
				flags.add(waitForElementPresent(countrySearch,
						"search country", 30));
				flags.add(JSClick(countrySearch, "Search country"));
				flags.add(waitForElementPresent(placeResults, "place results",
						30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults,
						"place results")) > 0, " Place results are not shown")));
				break;

			case "Flights":
				flags.add(JSClick(flightArrowSearch, "Flight search Arrow"));
				flags.add(waitForElementPresent(flightResults,
						"flight Results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(flightResults,
						"flight results")) > 0, " Flight results are not shown")));
				break;

			case "Travellers":
				flags.add(JSClick(travelersArrowSearch, "Flight search Arrow"));
				flags.add(waitForElementPresent(travelerResults,
						"traveler Results", 60));
				flags.add((assertTrue(Integer.parseInt(getText(travelerResults,
						"Traveler results")) > 0,
						" Traveler results are not shown")));
				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("select Search Type function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("select Search Type function execution failed");
		}
		return flag;
	}
	
	 * This method will select the checkbox of provided Traveller type and
	 * unchecks the remaining checkboxes Method Arguments can be: International
	 * or Domestic or Expatriate calling method eg:
	 * refineByTravellerType("Expatriate");
	 
	public boolean refineByTravellerType_TTMobile(String typeOption) throws Throwable {
		boolean result = true;
		LOG.info("refineByTravellerType method execution started ");

		List<Boolean> flags = new ArrayList<>();

		switch (typeOption) {
		case "International":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to check 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;

		case "Domestic":
			if (isElementNotSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to check 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("refineByTravellerType method execution completed");
		return result;
	}
	
	
	 * This functions will verify the traveler type for chosen location and 
	 * validates the count as per the filter option provided as param.
	 * 
	 
	public boolean VerifyTravlersTypeOfCountryLocation(String location, String type ) throws Throwable {
		boolean result = true;
		String locationType1 = "International";
		String locationType2 = "Domestic";

		LOG.info("VerifyTravlersTypeOfCountryLocation method execution started ");
		List<Boolean> flags = new ArrayList<>();
		flags.add(JSClick(createDynamicEle(TTMobilePage.location, location), "location"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
	    List<WebElement> internationalTravCounts = Driver.findElements(By.xpath("//td[text()='International']//preceding-sibling::td"));
	    List<WebElement> domeTravCounts = Driver.findElements(By.xpath("//td[text()='Domestic']//preceding-sibling::td"));
        
	    
	    if(type.equalsIgnoreCase(locationType1)){
	    
			for(WebElement internationalTrvCount:internationalTravCounts )
			{
				flags.add(assertTrue(Integer.parseInt(internationalTrvCount.getText().replace(",", ""))>=0,"Traveler count of International is greater than or equal to Zero."));

			}
			for(WebElement domeTravCount:domeTravCounts){
				flags.add(assertTrue(Integer.parseInt(domeTravCount.getText().replace(",", ""))==0,"Traveler count of Domestic is equal to Zero."));
				}
	    } else if(type.equalsIgnoreCase(locationType2))
       	 {
			for(WebElement internationalTrvCount:internationalTravCounts )
			   {
				flags.add(assertTrue(Integer.parseInt(internationalTrvCount.getText().replace(",", ""))==0,"Traveler count of International is equal to Zero."));
			}
			for(WebElement domesTravCount:domeTravCounts){
				flags.add(assertTrue(Integer.parseInt(domesTravCount.getText().replace(",", ""))>=0,"Traveler count of Domestic is equal to or greater than Zero."));
			   }
        }
	    if(isElementPresentWithNoException(goBtn))
        {
        	JSClick(goBtn, "Go Back button");
        	if(isElementPresentWithNoException(goBtn))
        	{
        		JSClick(goBtn, "Go Back button");
        	}
        }
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("VerifyTravlersTypeOfCountryLocation method execution completed");
		return result;
	}
	
	
	*//**
	  * @author: Arun
	  *  Implemented On : 20-10-2016
	  *  : This Component will fetch the Travelers count from the Application
	  *        The test case is 'Mobile_Locations_Presets_Search_Arriving'
	  *//*
	 public int getTravellersCountOfAlerts() throws Throwable {
		 boolean flag = true;
		 LOG.info("getTravellersCountOfAlerts function execution Started");

		 List<Boolean> flags = new ArrayList<>();
		 int total_count = 0;
		 String countryIndex = "";

		 // Fetching the number of Travelers
		 int count = 0;
		 String text;

		 //
		 List<WebElement> list = Driver
				 .findElements(TTMobilePage.alertCountriesCount);
		 for (WebElement c : list) {
			 count++;
		 }
		 LOG.info(count);
		 System.out.println("count : " + count);

		 int travellerCount[] = new int[count];
		 int listindex;
		 try {
			 for (int i = 1; i <= count; i++) {
				 LOG.info(i);
				 countryIndex = Integer.toString(i);
				 LOG.info(countryIndex);
				 List<WebElement> countryList = Driver
						 .findElements(createDynamicEle(
								 TTMobilePage.countryCount, countryIndex));
				 for (WebElement country : countryList) {

					 text = country.getText();
					 if (text.contains(",")) {
						 text = text.replace(",", "");
					 }
					 total_count = total_count + Integer.parseInt(text);
					 System.out.println(total_count);
				 }
			 }
			 
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 return total_count;
	}
*/
}