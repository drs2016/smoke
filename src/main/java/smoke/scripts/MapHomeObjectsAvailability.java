package smoke.scripts;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import bsh.commands.dir;
import smoke.objects.LoginPage;
import smoke.objects.MapHomePage;

public class MapHomeObjectsAvailability {
  
	public static WebDriver d;
	
  @BeforeClass
  public void beforeClass() {
	  
	  //Login 
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Deepti.Samantra\\Documents\\test_automation\\drivers\\chromedriver.exe");
		d = new ChromeDriver();
		d.manage().window().maximize();
		d.get("https://mobileqa.internationalsos.com/Mobile/MapUI/Login.aspx");
		d.findElement(LoginPage.userName).sendKeys("ITGQA@Internationalsos.com");
		d.findElement(LoginPage.password).sendKeys("Newyork@12345");
		d.findElement(LoginPage.loginButton).click();
		
  }
  
  @Test
  public static void mapObjectsAvailable(){
	  
	  //verify all objects visible
	  d.switchTo().frame("ctl00_MainContent_mapiframe");
	  assertTrue(AppTest.highlightElementIfDisplayed(MapHomePage.searchBox,d));
	  assertTrue(d.findElement(MapHomePage.searchOptionsDropDown).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.searchOptionsDropDownValues).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.filterPaneHeader).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.alertsPaneHeader).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.locationsPaneHeader).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.vismoPaneHeader).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.buildingsPaneHeader).isDisplayed());
	  assertTrue(d.findElement(MapHomePage.intlSOSPaneHeader).isDisplayed());
	  
  }
   
   @AfterClass
  public void afterClass() {
	  
	  //Logoff
	  // d.close();d.quit();
  }

}
