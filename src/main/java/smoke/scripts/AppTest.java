package smoke.scripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.collections.Lists;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import smoke.objects.MapHomePage;

public class AppTest
{
	public static String envName;
	public static String testSuiteName;
	public static String url;
	public static String userName;
	public static String pswd;
	
	//@Test
	public  static void main(String args[])throws IOException, ParserConfigurationException, SAXException{
		
		
		Properties prop = new Properties();
		InputStream input = null;
		
		input = new FileInputStream("prop.properties");

		// load a properties file
		prop.load(input);

		// get the property value and print it out
		//System.out.println(prop.getProperty("envName"));
			
		   envName=System.getProperty("envName");
		   testSuiteName= System.getProperty("testSuiteName");
		   
		   System.out.println( "POM Values" +envName + testSuiteName);
		   
			if(envName==null){
				envName=prop.getProperty("envName");
				
			}
			
			if(testSuiteName==null){
				testSuiteName=prop.getProperty("testSuiteName");
				
			}
			
			if(envName.equalsIgnoreCase("QA")){
				
				url=prop.getProperty("QAurl");
				userName=prop.getProperty("QAuserName");
				pswd=prop.getProperty("QApswd");
			}
			
			else if(envName.equalsIgnoreCase("StageUs")){
				
				url=prop.getProperty("StgUSurl");
				userName=prop.getProperty("StgUSuserName");
				pswd=prop.getProperty("StgUSpswd");
			}
			
			else if(envName.equalsIgnoreCase("StageFr")){
				
				url=prop.getProperty("StgFRurl");
				userName=prop.getProperty("StgFRuserName");
				pswd=prop.getProperty("StgFRpswd");
			}
			
			else if(envName.equalsIgnoreCase("Pen")){
				
				url=prop.getProperty("Penurl");
				userName=prop.getProperty("PenuserName");
				pswd=prop.getProperty("Penpswd");
			}

		
			System.out.println(envName);
			System.out.println(testSuiteName);
			System.out.println(url);
			System.out.println(userName);
			System.out.println(pswd);
		
		
		/*TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		List<String> suites = Lists.newArrayList();
		suites.add(System.getProperty("user.dir")+"\\"+testSuiteName);//path to xml..
		//suites.add("c:/tests/testng2.xml");
		testng.setTestSuites(suites);
		File file = new File(Common_Methods.outputDir);
		file.mkdirs();

		Reporter.log("Current Project Results Directory: "
				+ Common_Methods.outputDir, true);
		tng.setOutputDirectory(Common_Methods.outputDir);
		testng.run();*/
		
		
		/*//File file = new File((System.getProperty("user.dir")+"\\"+testSuiteName));
		XmlSuite xmlName =;
		List<XmlSuite> suite_a = new ArrayList<XmlSuite>();
		   suite_a.add(file);*/
		   TestNG tng = new TestNG();
		   tng.setXmlSuites((List<XmlSuite>) new Parser(System.getProperty("user.dir")+"\\"+testSuiteName).parse());
		   tng.run();
	}
	
	
	public static boolean highlightElementIfDisplayed(By by, WebDriver driver) {
		
		boolean isPresent=false;
		try{
		WebElement element;
		Thread.sleep(20000);
		element= driver.findElement(by);
		element.isDisplayed();
		isPresent =true;
		
		String originalStyle = element.getAttribute("style");
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
		
		try {
			Thread.sleep(1500);
		} 
		catch (InterruptedException e) {}
		
		js.executeScript("arguments[0].setAttribute('style', '" + originalStyle + "');", element);
		
		
		
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
		return isPresent;
	}
	
	
}