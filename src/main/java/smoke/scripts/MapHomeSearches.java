package smoke.scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import bsh.This;
import smoke.objects.LoginPage;

public class MapHomeSearches extends AppTest{
 
   WebDriver d;
	
	 @BeforeClass
	  public void beforeClass() {
		  
		  //Login 
			
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Deepti.Samantra\\Documents\\test_automation\\drivers\\chromedriver.exe");
			WebDriver d = new ChromeDriver();
			d.manage().window().maximize();
			System.out.println("STARTTTTTT");
			System.out.println(envName);
			System.out.println(testSuiteName);
			System.out.println(AppTest.url);
			System.out.println(AppTest.userName);
			System.out.println(AppTest.pswd);
			System.out.println("FINISSSSS");
			
			
			d.get(url);
			d.findElement(LoginPage.userName).sendKeys(userName);
			d.findElement(LoginPage.password).sendKeys(pswd);
			d.findElement(LoginPage.loginButton).click();
			
	  }
	  
	  @Test
	  public static void mapSearches(){
		  
		  //verify all objects visible
		  System.out.println("mapSearches");
		  
	  }
	   
	   @AfterClass
	  public void afterClass() {
		  
		  //Logoff
		  d.close();d.quit();
	  }

}
